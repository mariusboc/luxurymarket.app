﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin')
        .config(routeConfigs);

    routeConfigs.$inject = ['$stateProvider'];

    function routeConfigs($stateProvider) {

        $stateProvider
            .state('admin.dashboard', {
                url: "/",
                templateUrl: "src/templates/admin/dashboard.html"
            });
    }

})(window.angular);