﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.menu')
        .controller('AdminMenuController', AdminMenuController);

    AdminMenuController.$inject = [];

    function AdminMenuController() {
        var vm = this;

        vm.items = [
            {
                label: 'Dashboard',
                iconCls: 'fa fa-tachometer',
                path: '#/admin/'
            },
            {
                label: 'My Account',
                iconCls: 'fa fa-user',
                children: [
                    {
                        label: 'Child 1',
                        iconCls: 'fa fa-tachometer',
                        path: '#/admin/'
                    },
                    {
                        label: 'Child 2',
                        iconCls: 'fa fa-tachometer',
                        path: '#/admin/'
                    }
                ]
            }, {
                label: 'Offers',
                iconCls: 'fa fa-tag',
                children: [
                    {
                        label: 'Create new offer',
                        path: '#/admin/new-offer/upload'
                    },
                    {
                        label: 'Manage offers',
                        path: '#/admin/offers'
                    }
                ]
            },
            {
                label: 'Orders',
                iconCls: 'fa fa-shopping-cart',
                children: [
                    {
                        label: 'Create new offer',
                        path: '#/admin/'
                    },
                    {
                        label: 'View current offers',
                        path: '#/admin/'
                    },
                    {
                        label: 'View expired offers',
                        path: '#/admin/'
                    }
                ]
            },
            {
                label: 'Reports',
                iconCls: 'fa fa-pie-chart',
                children: [
                    {
                        label: 'Create new offer',
                        path: '#/admin/'
                    },
                    {
                        label: 'View current offers',
                        path: '#/admin/'
                    },
                    {
                        label: 'View expired offers',
                        path: '#/admin/'
                    }
                ]
            }
        ];

        // functions
        vm.menuItemClick = menuItemClick;

        function menuItemClick($event, item) {
            $event.preventDefault();

            if (item.open) {
                item.open = false;
                return;
            }

            angular.forEach(vm.items, function (menuItem) {
                menuItem.open = false;
            });

            item.open = !item.open;
        }
    }

})(window.angular);