﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.offer')
        .config(routeConfigs);

    routeConfigs.$inject = ['$stateProvider'];

    function routeConfigs($stateProvider) {

        $stateProvider
         .state('admin.new-offer', {
             url: "/new-offer",
             abstract: true,
             templateUrl: "src/templates/admin/offer/newoffer-index.html",
             controller: 'NewOfferController',
             controllerAs: 'newoffer'
         })
        .state('admin.new-offer.upload', {
            url: "/upload",
            templateUrl: "src/templates/admin/offer/upload-offer.html",
            controller: 'UploadNewOfferController'
        })
        .state('admin.new-offer.details', {
            url: "/details",
            templateUrl: "src/templates/admin/offer/offer-details.html",
            controller: 'NewOfferDetailsController',
            controllerAs: 'offerdetails',
            params: {
                details: null,
                offerId: null
            }
        })
        .state('admin.new-offer.product-details', {
            url: "/product-details",
            templateUrl: "src/templates/admin/offer/offer-product-details.html",
            controller: 'NewOfferProductDetailsController',
            controllerAs: 'productdetails',
            params: {
                details: null,
                offerId: null,
                productDetails: null
            }
        })
        .state('admin.new-offer.product-details-error', {
            url: "/report-error",
            templateUrl: "src/templates/admin/offer/report-error.html",
            controller: 'NewOfferProductErrorController',
            controllerAs: 'producterror',
            params: {
                details: null,
                offerId: null,
                productDetails: null
            }
        })
        .state('admin.new-offer.documents', {
            url: "/supporting-documents",
            templateUrl: "src/templates/admin/offer/support-documents.html",
            controller: 'NewOfferDocumentsController',
            params: {
                details: null,
                offerId: null
            }
        })
        .state('admin.new-offer.summary', {
            url: "/summary",
            templateUrl: "src/templates/admin/offer/offer-summary.html",
            controller: 'NewOfferSummaryController',
            controllerAs: 'offersummary',
            params: {
                details: null,
                offerId: null
            }
        })
        .state('admin.new-offer.published', {
            url: "/published",
            templateUrl: "src/templates/admin/offer/offer-published.html"
        });
    }

})(window.angular);