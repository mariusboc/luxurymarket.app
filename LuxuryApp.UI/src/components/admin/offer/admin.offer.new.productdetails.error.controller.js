﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.offer')
        .controller('NewOfferProductErrorController', NewOfferProductErrorController);

    NewOfferProductErrorController.$inject = ['_', '$stateParams', '$state'];

    function NewOfferProductErrorController(_, $stateParams, $state) {
        var vm = this;

        if ($stateParams.offerId === null) {
            $state.go('admin.new-offer.upload');
        }

        // functions
        vm.back = back;

        function back($event, type) {
            $event.preventDefault();
            switch (type) {
                case 1:
                    $state.go('admin.new-offer.details', $stateParams);
                    break;
                case 2:
                    $state.go('admin.new-offer.product-details', $stateParams);
                    break;
            }
        }

    }

})(window.angular);