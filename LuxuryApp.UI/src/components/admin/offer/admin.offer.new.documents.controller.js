﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.offer')
        .controller('NewOfferDocumentsController', NewOfferDocumentsController);

    NewOfferDocumentsController.$inject = ['$scope', '_', '$stateParams', '$state', 'Upload'];

    function NewOfferDocumentsController($scope, _, $stateParams, $state, Upload) {

        if ($stateParams.offerId === null) {
            $state.go('admin.new-offer.upload');
        }

        // update steps
        $scope.$emit('updateSteps', { number: 3, complete: false, active: true });

        // functions
        $scope.upload = upload;
        $scope.continue = continueProcess;
        $scope.back = back;

        $scope.$watch('files', function () {
            $scope.upload($scope.files);
        });

        function continueProcess($event) {
            // todo - some form of validation
            $scope.$emit('updateSteps', { number: 3, complete: true, active: true });
            $state.go('admin.new-offer.summary', { offerId: -1, details: $stateParams.details });
        }

        function back($event) {
            $event.preventDefault();
            $state.go('admin.new-offer.details', { offerId: $stateParams.offerId, details: $stateParams.details });
        }

        function upload(files) {
            if (files && files.length) {
                $scope.files = files;
                angular.forEach(files, function (file) {
                    file.upload = Upload.upload({
                        url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
                        data: { file: file }
                    });

                    file.upload.then(function (response) {
                        // done
                    }, function (response) {
                        // error
                    }, function (evt) {
                        file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                    });
                });
            }
        }
    }

})(window.angular);