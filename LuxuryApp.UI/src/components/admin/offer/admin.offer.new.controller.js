﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.offer')
        .controller('NewOfferController', NewOfferController);

    NewOfferController.$inject = ['$scope', '_'];

    function NewOfferController($scope, _) {
        var vm = this;

        // view models
        // we need to keep the steps in this order
        vm.steps = [
            {
                name: 'Upload Documents',
                number: 1,
                active: false,
                complete: false
            },
            {
                name: 'Offer Details',
                number: 2,
                active: false,
                complete: false
            },
            {
                name: 'Supporting Documents',
                number: 3,
                active: false,
                complete: false
            },
            {
                name: 'Offer Summary',
                number: 4,
                active: false,
                complete: false
            }
        ];

        // events
        $scope.$on('updateSteps', _updateSteps);

        // private functions
        function _updateSteps(event, data) {
            angular.forEach(vm.steps, function (step) {
                if (step.number > data.number) {
                    // disable future steps
                    step.complete = false;
                    step.active = false;
                }
                else if (step.number < data.number) {
                    // previous steps
                    step.complete = true;
                    step.active = true;
                }
                else {
                    // this is current step
                    step.complete = data.complete;
                    step.active = data.active;
                }
            });
        }
    }

})(window.angular);