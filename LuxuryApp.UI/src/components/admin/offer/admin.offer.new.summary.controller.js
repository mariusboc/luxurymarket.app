﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.offer')
        .controller('NewOfferSummaryController', NewOfferSummaryController);

    NewOfferSummaryController.$inject = ['$scope', '_', '$stateParams', '$state'];

    function NewOfferSummaryController($scope, _, $stateParams, $state) {
        var vm = this;

        if ($stateParams.offerId === null) {
            $state.go('admin.new-offer.upload');
        }

        // update steps
        $scope.$emit('updateSteps', { number: 4, complete: false, active: true });

        // functions
        vm.back = back;
        vm.publish = publish;

        function back($event) {
            $event.preventDefault();
            $state.go('admin.new-offer.documents', { offerId: $stateParams.offerId, details: $stateParams.details });
        }

        function publish($event) {
            $state.go('admin.new-offer.published');
        }
    }

})(window.angular);