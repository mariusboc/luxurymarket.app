﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.offer')
        .controller('NewOfferDetailsController', NewOfferDetailsController);

    NewOfferDetailsController.$inject = ['$scope', '_', '$stateParams', '$state'];

    function NewOfferDetailsController($scope, _, $stateParams, $state) {
        var vm = this, _currentFilter = null;

        if ($stateParams.details === null) {
            $state.go('admin.new-offer.upload');
        }

        // update steps
        $scope.$emit('updateSteps', { number: 2, complete: false, active: true });

        // process products
        angular.forEach($stateParams.details.items, function (product) {
            product.isLineBuy = false;
            product.include = true;
        });

        // view variables
        vm.products = $stateParams.details.items;
        vm.allLineBuy = false;

        // init filters
        _initFilters(vm.products);

        // functions
        vm.continue = continueProcess;
        vm.selectAllLineBuy = selectAllLineBuy;
        vm.clickProductLineBuy = clickProductLineBuy;
        vm.clickIncludeExclude = clickIncludeExclude;
        vm.filterBy = filterBy;
        vm.isStatus = isStatus;
        vm.viewProductDetails = viewProductDetails;

        function continueProcess($event) {
            // todo - some form of validation
            $scope.$emit('updateSteps', { number: 2, complete: true, active: true });
            $state.go('admin.new-offer.documents', { offerId: -1, details: vm.products });
        }

        function selectAllLineBuy($event) {
            angular.forEach(vm.products, function (product) {
                product.isLineBuy = vm.allLineBuy;
            });
        }

        function clickProductLineBuy($event) {
            var selectedCount = _.filter(vm.products, function (product) {
                return product.isLineBuy;
            }).length;

            vm.allLineBuy = selectedCount === vm.products.length;
        }

        function clickIncludeExclude($event, product) {
            product.include = !product.include;
        }

        function _initFilters(products) {
            if (products === null) {
                return;
            }

            var productsWithErrors = _.where(products, { hasErrors: true });
            var productsWithErorsCount = (productsWithErrors || []).length;

            vm.filters = [
            {
                label: 'all',
                count: products.length,
                active: true,
                cssClass: '',
                type: 1
            },
            {
                label: 'successfull uploaded',
                count: products.length - productsWithErorsCount,
                active: false,
                cssClass: '',
                type: 2
            },
            {
                label: 'with errors',
                count: productsWithErorsCount,
                active: false,
                cssClass: 'message-error',
                type: 3
            }];

            _currentFilter = vm.filters[0];
        }

        function filterBy($event, filter) {
            $event.preventDefault();

            var currentFilter = _.find(vm.filters, function (f) {
                return f.type === filter.type;
            });

            angular.forEach(vm.filters, function (f) {
                f.active = false;
            });

            filter.active = true;
            _currentFilter = filter;
        }

        function isStatus(product) {
            switch (_currentFilter.type) {
                case 1:
                    // all
                    return true;
                case 2:
                    // only successfull
                    return !product.hasErrors;
                case 3:
                    // only with errors
                    return product.hasErrors;
            }
        }

        function viewProductDetails($event, product) {
            $state.go('admin.new-offer.product-details', {
                details: vm.products,
                offerId: $stateParams.offerId,
                productDetails: product
            });
        }

    }

})(window.angular);