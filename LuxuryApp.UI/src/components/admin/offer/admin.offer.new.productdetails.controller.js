﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin.offer')
        .controller('NewOfferProductDetailsController', NewOfferProductDetailsController);

    NewOfferProductDetailsController.$inject = ['_', '$stateParams', '$state'];

    function NewOfferProductDetailsController(_, $stateParams, $state) {
        var vm = this;

        if ($stateParams.offerId === null) {
            $state.go('admin.new-offer.upload');
        }

        // functions
        vm.reportError = reportError;
        vm.back = back;

        function reportError($event) {
            $state.go('admin.new-offer.product-details-error', $stateParams);
        }

        function back($event) {
            $event.preventDefault();
            $state.go('admin.new-offer.details', $stateParams);
        }

    }

})(window.angular);