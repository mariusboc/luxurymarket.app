﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.admin', [
        'luxurymarket.components.admin.menu',
        'luxurymarket.components.admin.offer',
        'ui.router'
    ]);

})(window.angular);