﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.auth')
        .config(routeConfigs);

    routeConfigs.$inject = ['$stateProvider'];

    function routeConfigs($stateProvider) {

        $stateProvider
            .state('auth.login', {
                url: "/login",
                templateUrl: "src/templates/auth/login.html",
                controller: "LoginController",
                controllerAs: "login"
            })
            .state('auth.register', {
                url: "/register",
                templateUrl: "src/templates/auth/register.html",
                controller: "RegisterController",
                controllerAs: "register"
            })
            .state('auth.forgot-password', {
                url: "/forgot-password",
                templateUrl: "src/templates/auth/forgot-password.html",
                controller: 'ForgotPasswordController',
                controllerAs: 'forgotpass'
            })
            .state('auth.terms', {
                url: "/terms",
                templateUrl: "src/templates/auth/terms.html"
            })
            .state('auth.message', {
                url: "/message",
                templateUrl: "src/templates/auth/message.html",
                controller: 'MessageController',
                controllerAs: 'msg',
                params: {
                    type: null
                }
            });
    }

})(window.angular);