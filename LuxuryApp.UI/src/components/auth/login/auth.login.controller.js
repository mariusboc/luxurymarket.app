﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.auth.login')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['AuthenticationService', '$state', 'localStorageService'];

    function LoginController(AuthenticationService, $state, localStorageService) {
        var vm = this;

        // view model
        vm.loginModel = {
            "userName": null,
            "password": null,
            "rememberMe": true
        };

        // view variables
        vm.hasError = false;
        vm.error_message = null;
        vm.isLoading = false;

        // functions
        vm.login = login;

        function login() {
            if (vm.loginForm.$invalid) {
                return;
            }

            vm.isLoading = true;
            localStorageService.clearAll();
            AuthenticationService.getToken(vm.loginModel).then(function (response) {
                vm.isLoading = false;
                if (response.success) {
                    $state.go('site.home');
                }
                else {
                    vm.hasError = true;
                    vm.error_message = response.error_message;
                }
            });
        }

    }
})(window.angular);