﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.auth.register')
        .controller('RegisterController', RegisterController);

    RegisterController.$inject = ['AuthenticationService', 'localStorageService', '$state', 'MESSAGE_TYPES', 'LocationService'];

    function RegisterController(AuthenticationService, localStorageService, $state, MESSAGE_TYPES, LocationService) {
        var vm = this;

        // variables

        // view model
        vm.requestModel = {
            contactName: null,
            companyName: null,
            companyType: 1,
            city: null,
            zipCode: null,
            streetAddress: null,
            stateId: null,
            countryId: null,
            retailId: null,
            website: null,
            phoneNumber: null,
            email: null,
            reemail: null,
            description: null,
            agreeTC: false
        };

        // view variables
        vm.isLoading = false;
        vm.hasError = false;
        vm.error_message = null;
        vm.countries = null;
        vm.states = [{
            stateId: null,
            stateName: 'STATE or OTHER'
        }];

        // init
        _init();

        // functions
        vm.requestNewAccount = requestNewAccount;
        vm.updateStates = updateStates;

        function requestNewAccount() {
            if (vm.registerForm.$invalid) {
                return;
            }

            vm.isLoading = true;
            localStorageService.clearAll();
            AuthenticationService.requestNewAccount(vm.requestModel).then(function (response) {
                vm.isLoading = false;
                if (response.success) {
                    $state.go('auth.message', {
                        type: MESSAGE_TYPES.RequestSent
                    });
                }
                else {
                    vm.hasError = true;
                    vm.error_message = response.error_message;
                }
            });
        }

        function updateStates() {
            vm.requestModel.stateId = null;
            LocationService.getStates(vm.requestModel.countryId).then(function (response) {
                if (response.success) {
                    response.data.splice(0, 0, {
                        stateId: null,
                        stateName: 'STATE or OTHER'
                    });
                    vm.states = response.data;
                }
                else {
                    vm.states = [{
                        stateId: null,
                        stateName: 'STATE or OTHER'
                    }];
                }
            });
        }

        // private functions
        function _init() {
            LocationService.getCountries().then(function (response) {
                if (response.success) {
                    response.data.splice(0, 0, {
                        id: null,
                        countryName: 'COUNTRY'
                    });
                    vm.countries = response.data;
                }
                else {
                    vm.countries = [{
                        id: null,
                        countryName: 'COUNTRY'
                    }];
                }
            });
        }
    }
})(window.angular);