﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components', [
        'luxurymarket.components.auth',
        'luxurymarket.components.site',
        'luxurymarket.components.admin'
    ]);

})(window.angular);