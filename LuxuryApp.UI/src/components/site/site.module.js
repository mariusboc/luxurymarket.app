﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.site', [
        'luxurymarket.components.site.home',
        'luxurymarket.components.site.product',
        'luxurymarket.components.site.cart',
        'luxurymarket.components.site.menu',
        'ui.router'
    ]);

})(window.angular);