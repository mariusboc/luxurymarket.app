﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.site.menu')
        .controller('SiteMenuController', SiteMenuController);

    SiteMenuController.$inject = ['$state', '$rootScope', '_'];

    function SiteMenuController($state, $rootScope, _) {
        var vm = this;

        // menu items
        vm.items = [
            {
                label: 'Featured',
                state: 'site.home',
                active: true
            },
            {
                label: 'Women\'s Apparel',
                state: 'site.products',
                params: {
                    header: 'Women\'s Shop'
                }
            },
            {
                label: 'Handbags / Accesories',
                state: 'site.products',
                params: {
                    header: 'Handbags / Accesories'
                }
            },
            {
                label: 'Men\'s Shop',
                state: 'site.products',
                params: {
                    header: 'Men\'s Shop'
                }
            },
            {
                label: 'Brands',
                state: 'site.brands'
            }
        ];

        // functions
        vm.menuItemClick = menuItemClick;

        function menuItemClick($event, item) {
            $event.preventDefault();

            if (item.active) {
                return;
            }

            angular.forEach(vm.items, function (menuItem) {
                menuItem.active = false;
            });

            item.active = !item.active;
            $state.go(item.state, item.params);
        }
    }
})(window.angular);