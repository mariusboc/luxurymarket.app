﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.site.product')
        .controller('ProductsListController', ProductsListController);

    ProductsListController.$inject = ['$stateParams'];

    function ProductsListController($stateParams) {
        var vm = this;

        vm.header = $stateParams.header || "Women's Shop";

    }
})(window.angular);