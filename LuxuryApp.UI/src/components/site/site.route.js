﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.site')
        .config(routeConfigs);

    routeConfigs.$inject = ['$stateProvider'];

    function routeConfigs($stateProvider) {

        $stateProvider
            .state('site.home', {
                url: "/home",
                templateUrl: "src/templates/site/home.html",
                controller: "HomeController",
                controllerAs: "home"
            })
            .state('site.products', {
                url: "/products",
                templateUrl: "src/templates/site/products-list.html",
                controller: "ProductsListController",
                controllerAs: "products",
                params: {
                    header: null
                },
            })
            .state('site.product-details', {
                url: "/products/:id",
                templateUrl: "src/templates/site/product-details.html",
                controller: "ProductDetailsController",
                controllerAs: "pd"
            })
            .state('site.cart', {
                url: "/cart",
                templateUrl: "src/templates/site/cart-items.html",
                controller: "CartController",
                controllerAs: "cart"
            })
            .state('site.brands', {
                url: "/brands",
                templateUrl: "src/templates/site/brands.html"
            });
    }

})(window.angular);