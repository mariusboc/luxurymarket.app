﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.components.site.cart')
        .controller('CartController', CartController);

    CartController.$inject = ['_'];

    function CartController(_) {
        var vm = this;

        // view variables
        vm.isOpen = false;
        vm.products = [
            {
                id: 1,
                img: '/assets/images/products/small-shoes-1.jpg',
                name: 'Navy Leather Sneaker',
                brand: 'Gucci',
                price: '567.99',
                discount: '50%',
                totalCost: '435,342.99',
                category: 'Shoes',
                productId: '3983AB',
                material: 'Leather',
                color: 'Gold',
                season: 'Summer',
                inactive: false,
                sellers: [
                    {
                        id: 1,
                        name: 'A123',
                        units: 87,
                        unitPrice: '123.99',
                        totalCost: '1,487.88',
                        active: true
                    },
                    {
                        id: 2,
                        name: 'A124',
                        units: 56,
                        unitPrice: '125.99',
                        totalCost: '1,787.88',
                        active: true
                    },
                    {
                        id: 3,
                        name: 'A125',
                        units: 23,
                        unitPrice: '119.99',
                        totalCost: '1,387.88',
                        active: true
                    }
                ]
            },
            {
                id: 2,
                img: '/assets/images/products/e.jpg',
                name: 'Italian Leather Sneaker',
                brand: 'Cherri Bellini',
                price: '120.99',
                discount: '50%',
                totalCost: '435,342.99',
                category: 'Shoes',
                productId: 'X2efAB',
                material: 'Leather',
                color: 'Gold',
                season: 'Summer',
                inactive: false,
                sellers: [
                    {
                        id: 1,
                        name: 'A123',
                        units: 87,
                        unitPrice: '123.99',
                        totalCost: '1,487.88',
                        active: true
                    },
                    {
                        id: 2,
                        name: 'A124',
                        units: 56,
                        unitPrice: '125.99',
                        totalCost: '1,787.88',
                        active: true
                    }
                ]
            },
            {
                id: 3,
                img: '/assets/images/products/b.jpg',
                name: 'Metallic Leather Coat',
                brand: 'Gucci',
                price: '695.99',
                discount: '75%',
                totalCost: '435,342.99',
                category: 'Coat',
                productId: '43fsAS3',
                material: 'Leather',
                color: 'Black',
                season: 'Summer',
                inactive: false,
                sellers: [
                    {
                        id: 1,
                        name: 'A123',
                        units: 20,
                        unitPrice: '695.99',
                        totalCost: '13,919.88',
                        active: true
                    }
                ]
            },
            {
                id: 5,
                img: '/assets/images/bag_03.jpg',
                name: 'Black Bag',
                brand: 'Dolce & Gabbana',
                price: '695.99',
                discount: '45%',
                totalCost: '435,342.99',
                category: 'Bags',
                productId: 'DC123SA',
                material: 'Leather',
                color: 'Black',
                season: 'Summer',
                inactive: false,
                sellers: [
                    {
                        id: 10,
                        name: 'B32D',
                        units: 100,
                        unitPrice: '400.00',
                        totalCost: '4,000.00',
                        active: true
                    },
                    {
                        id: 11,
                        name: 'D43SD43',
                        units: 10,
                        unitPrice: '390.00',
                        totalCost: '3,900.00',
                        active: true
                    }
                ]
            },
            {
                id: 4,
                img: '/assets/images/products/a.jpg',
                name: 'White Leather Coat',
                brand: 'Alexander McQueen',
                price: '695.99',
                discount: '50%',
                totalCost: '435,342.99',
                category: 'Coat',
                productId: 'f332AMQ',
                material: 'Leather',
                color: 'White',
                season: 'Summer',
                inactive: false,
                sellers: [
                    {
                        id: 1,
                        name: 'A123',
                        units: 20,
                        unitPrice: '123.99',
                        totalCost: '1,487.88',
                        active: true
                    }
                ]
            }
        ];
        vm.groupByType = 'default';
        vm.groupingField = null;


        // functions
        vm.open = open;
        vm.setSellerActive = setSellerActive;
        vm.groupBy = groupBy;

        function open($event, product) {
            if (typeof product === 'undefined') {
                product.isOpen = false;
            }

            product.isOpen = !product.isOpen;
        }

        function setSellerActive($event, product, seller) {
            seller.active = !seller.active;

            if (!seller.active) {
                var activeSellers = _.find(product.sellers, function (s) {
                    return s.active;
                });

                var activeSellersCounts = (activeSellers || []).length;
                if (activeSellersCounts === 0) {
                    product.inactive = true;
                }
            }
            else {
                product.inactive = false;
            }
        }

        function groupBy($event, type) {
            $event.preventDefault();

            if (type === vm.groupByType) {
                return;
            }

            vm.groupByType = type;
            switch (type) {
                case 'default':
                    vm.groupingField = null;
                    break;

                case 'category':
                    vm.groupingField = 'category';
                    break;

                case 'brand':
                    vm.groupingField = 'brand';
                    break;
            }
        }
    }
})(window.angular);