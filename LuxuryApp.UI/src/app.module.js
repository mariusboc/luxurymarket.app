﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket', [
        'luxurymarket.components',
        'luxurymarket.services',
        'luxurymarket.directives',
        'luxurymarket.libs',
        'ui.router',
        'ngMessages',
        'ngFileUpload',
        'LocalStorageModule',
        'angular.filter'
    ]);

})(window.angular);