﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.services.interceptors')
        .factory('authInterceptorService', authInterceptorService);

    authInterceptorService.$inject = ['$q', 'localStorageService', '$injector'];

    function authInterceptorService($q, localStorageService, $injector) {

        var _request = function (config) {

            config.headers = config.headers || {};

            var authData = localStorageService.get('authorizationData');
            if (authData && authData.token) {
                config.headers.Authorization = 'Bearer ' + authData.token;
            }

            return config;
        };

        var _responseError = function (rejection) {
            if (rejection.status === 401) {
                var authService = $injector.get('AuthenticationService');
                var authData = localStorageService.get('authorizationData');

                var state = $injector.get('$state');
                state.go('auth.login');
            }
            return $q.reject(rejection);
        };

        return {
            request: _request,
            responseError: _responseError
        };
    }

})(window.angular);