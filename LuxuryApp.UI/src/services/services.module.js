﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.services', [
        'luxurymarket.services.interceptors'
    ]);

})(window.angular);