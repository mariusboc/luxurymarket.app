﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.services')
        .factory('OfferService', OfferService);

    OfferService.$inject = ['$http', 'Upload', 'SERVICES_CONFIG'];

    function OfferService($http, Upload, SERVICES_CONFIG) {

        var svc = {
            uploadNewOffer: uploadNewOffer
        };

        return svc;

        function uploadNewOffer(file) {
            return Upload.upload({
                url: SERVICES_CONFIG.apiUrl + '/api/offers/upload',
                file: file,
            }).then(function (response) {
                if (response.status === 200) {
                    return { success: true, data: response.data };
                }
                else {
                    return { success: false, error_message: 'There was an error during upload. Please try again' };
                }
            }, function (response) {
                return { success: false, error_message: response.data.Message };
            });
        }
    }
})(window.angular);