﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.services')
        .factory('AuthenticationService', AuthenticationService);

    AuthenticationService.$inject = ['$http', 'localStorageService', 'SERVICES_CONFIG'];

    function AuthenticationService($http, localStorageService, SERVICES_CONFIG) {

        var svc = {
            requestNewAccount: requestNewAccount,
            getToken: getToken,
            requestPasswordChange: requestPasswordChange,
            fillAuthData: fillAuthData,
            logout: logout,
            isLoggedIn: isLoggedIn
        };

        // private vars
        var _authentication = {
            isAuth: false,
            userName: null
        };

        return svc;

        // functions
        function requestNewAccount(model) {
            return $http.post(SERVICES_CONFIG.apiUrl + '/api/registrationRequests', model)
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true };
                    }
                    else {
                        return { success: false, error_message: 'Please try again' };
                    }
                }, function (response) {
                    return { success: false, error_message: response.data.exceptionMessage };
                });
        }

        function getToken(model) {
            var data = "grant_type=password&username=" + model.userName + "&password=" + model.password;

            return $http.post(SERVICES_CONFIG.authServiceUrl + '/token', data, {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).then(function (response) {
                if (response.status === 200) {

                    // additional logic
                    localStorageService.set('authorizationData', { token: response.data.access_token, userName: response.data.userName });

                    _authentication.isAuth = true;
                    _authentication.userName = response.data.userName;

                    return { success: true, data: response.data };
                }
                else {
                    return { success: false, error_message: response.data.error_description };
                }
            }, function errorCallback(response) {
                return { success: false, error_message: response.data.error_description };
            });
        }

        function requestPasswordChange(model) {
            return $http.post(SERVICES_CONFIG.authServiceUrl + '/api/accounts/forgotPassword', model)
                .then(function (response) {
                    if (response.status === 200) {
                        return { success: true };
                    }
                    else {
                        return { success: false, error_message: 'Please try again' };
                    }
                }, function (response) {
                    return { success: false, error_message: response.data.exceptionMessage };
                });
        }

        function fillAuthData() {
            var authData = localStorageService.get('authorizationData');
            if (authData) {
                _authentication.isAuth = true;
                _authentication.userName = authData.userName;
            }
        }

        function logout() {
            _authentication = {
                isAuth: false,
                userName: null
            };
            localStorageService.clearAll();
        }

        function isLoggedIn() {
            return _authentication.isAuth;
        }
    }

})(window.angular);