﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket.libs', [])
    .factory('_', ['$window', function ($window) {
        return $window._; // assumes underscore has already been loaded on the page
    }]);
})(window.angular);