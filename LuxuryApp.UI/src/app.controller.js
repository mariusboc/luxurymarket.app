﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket')
        .controller('MainController', MainController);

    MainController.$inject = ['AuthenticationService', '$state'];

    function MainController(AuthenticationService, $state) {
        var vm = this;

        // functions
        vm.logout = logout;

        function logout($event) {
            $event.preventDefault();
            AuthenticationService.logout();
            $state.go('auth.login');
        }
    }

})(window.angular);