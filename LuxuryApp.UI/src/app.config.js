﻿(function (angular) {

    'use strict';

    angular.module('luxurymarket')
        .run(['AuthenticationService', '$rootScope', '$state', function (AuthenticationService, $rootScope, $state) {
            AuthenticationService.fillAuthData();

            $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                if (!AuthenticationService.isLoggedIn()) {
                    if (!toState.name.startsWith("auth.")) {
                        event.preventDefault();
                        $state.go('auth.login');
                    }
                    else {
                        // do nothing
                    }
                }
                else {
                    if (toState.name.startsWith("auth.")) {
                        $state.go('site.home');
                    }
                    else {
                        // do nothing
                    }
                }
            });
        }])
        .config(['localStorageServiceProvider', '$httpProvider', '$locationProvider', function (localStorageServiceProvider, $httpProvider, $locationProvider) {
            // setup prefix of local storage
            localStorageServiceProvider.setPrefix('ls.luxurymarket');

            // inject interceptors
            $httpProvider.interceptors.push('authInterceptorService');

            // remove # from urls- TODO
            //$locationProvider.html5Mode({
            //    enabled: true,
            //    requireBase: false
            //});
        }])
        .constant('SERVICES_CONFIG', {
            authServiceUrl: 'http://testing.qa.auth',
            apiUrl: 'http://testing.qa.api'
        })
        .constant('MESSAGE_TYPES', {
            RequestSent: 1,
            ForgotPassword: 2
        });

})(window.angular);