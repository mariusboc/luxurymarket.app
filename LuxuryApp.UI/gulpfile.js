﻿var gulp = require('gulp'),
    jshint = require('gulp-jshint'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    livereload = require('gulp-livereload'),
    sass = require('gulp-sass'),
    clean = require('gulp-clean');

// Fonts
var fonts = {
    in: [
        'bower_components/bootstrap/dist/fonts/**/*',
        'bower_components/font-awesome/fonts/*'
    ],
    out: 'dist/fonts/'
};

// CSS source file: .scss files
var css = {
    in: [
        'assets/stylesheets/main.scss'
    ],
    out: 'dist/css/',
    watch: 'assets/stylesheets/**/*',
    sassOpts: {
        outputStyle: 'nested',
        precison: 3,
        errLogToConsole: true
    }
};

var scripts = [
            'bower_components/jquery/dist/jquery.js',
            'bower_components/bootstrap/dist/js/bootstrap.js',
            'bower_components/angular/angular.js',
            'bower_components/angular-ui-router/release/angular-ui-router.js',
            'bower_components/angular-messages/angular-messages.js',
            'bower_components/angular-local-storage/dist/angular-local-storage.js',
            'bower_components/angular-filter/dist/angular-filter.js',
            'bower_components/underscore/underscore.js',
            'bower_components/ng-file-upload-shim/ng-file-upload-shim.js',
            'bower_components/ng-file-upload/ng-file-upload.js',
            'src/**/*.module.js',
            'src/**/*.route.js',
            'src/**/*.config.js',
            'src/**/*.interceptor.js',
            'src/**/*.service.js',
            'src/**/*.controller.js',
            'src/**/*.directive.js',
            'src/**/libs.*.js',
            '!src/templates/*.html'
];

// Copy bootstrap required fonts to dist
gulp.task('fonts', function () {
    return gulp
        .src(fonts.in)
        .pipe(gulp.dest(fonts.out));
});

// Compile scss
gulp.task('compile-styles', ['fonts'], function () {
    return gulp.src(css.in)
        .pipe(sass(css.sassOpts))
        .pipe(gulp.dest(css.out));
});

gulp.task('minify-styles', ['compile-styles'], function () {
    return gulp.src([
        'bower_components/bootstrap/dist/css/bootstrap.css',
        'bower_components/font-awesome/css/font-awesome.css',
        'dist/css/main.css'
    ]).pipe(concat('app.css'))
      .pipe(gulp.dest('dist/css'));
});

// Copy all image files to dist
gulp.task('copy-images-files', function () {
    return gulp.src('assets/images/**/*.{jpg,jpeg,png,gif}')
        .pipe(gulp.dest('dist/images'));
});

gulp.task('minify-js', function () {
    gulp.src(scripts)
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        //.pipe(gulpif(argv.production, uglify()))
        .pipe(concat('app.js'))
        .pipe(gulp.dest('dist/js'));
});

// Clean all files in dist
gulp.task('clean', function () {
    gulp.src('./dist/*')
        .pipe(clean({ force: true }));
});

// build task
gulp.task('build', [
    'minify-styles',
    'minify-js',
    'copy-images-files'
], function () {
    livereload.listen();

    gulp.watch(css.watch, ['minify-styles']);
    gulp.watch('src/**/*.js', ['minify-js']);
    gulp.watch('assets/images/**/*.{jpg,jpeg,png,gif}', ['copy-images-files']);
});