﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.DataAccess.Repository;

namespace LuxuryApp.Processors.Services
{
    public class RegistrationRequestService : IRegistrationRequestService
    {
        private readonly Repository _connection;

        public RegistrationRequestService()
        {
            _connection = new Repository();
        }

        public int CreateRequest(RegistrationRequest model)
        {
            if (model == null)
                throw new Exception("Invalid  Request");

            var parameters = new List<SqlParameter>
            {
              new SqlParameter("@ContactName", model.ContactName),
              new SqlParameter("@CompanyName", model.CompanyName),
              new SqlParameter("@CompanyTypeID", (Int16)model.CompanyType),
              new SqlParameter("@Email", model.Email),
              new SqlParameter("@Phone", model.PhoneNumber),
              new SqlParameter("@StreetAddress", model.StreetAddress),
              new SqlParameter("@RetailID", model.RetailId),
              new SqlParameter("@City",model.City),
              new SqlParameter("@StateID", model.StateId),
              new SqlParameter("@ZipCode", model.ZipCode),
              new SqlParameter("@CountryID", model.CountryId),
              new SqlParameter("@Website", model.Website),
            new SqlParameter("@Description", model.Description)

             };

            var returnValue = _connection.ExecuteProcedure(StoredProcedureNames.RegisterRequest_Insert, parameters.ToArray());

            return returnValue;
        }
    }
}
