﻿using LuxuryApp.Core.DataAccess.Repository;
using LuxuryApp.Processors.Helpers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LuxuryApp.Contracts.Models;

namespace LuxuryApp.Processors.Services
{
    public class CurrencyService
    {
        private readonly Repository _connection;

        public CurrencyService()
        {
            _connection = new Repository();
        }

        public List<Currency> GetById(int? id)
        {
            var parameters = new List<SqlParameter>
            {
                 new SqlParameter("@CurrencyID", id),
             };
            var reader = _connection.LoadData(StoredProcedureNames.CurrencyGetByID, parameters.ToArray());
            return reader.ToCurrencyList();
        }
    }
}
