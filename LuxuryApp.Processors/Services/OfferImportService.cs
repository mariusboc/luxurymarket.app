﻿using LuxuryApp.Contracts.Services;
using LuxuryApp.Core.DataAccess.Repository;
using LuxuryApp.Processors.Helpers;
using LuxuryApp.Processors.Import;
using LuxuryApp.Processors.Validation;
using LuxuryApp.Utils.Helpers;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using LuxuryApp.Processors.Resources.Import;
using System.Data.SqlClient;
using System.IO;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Enums;
using System;
using System.Configuration;

namespace LuxuryApp.Processors.Services
{
    public class OfferImportService : IOfferImportService
    {
        private readonly Dictionary<string, string> _offerResourceColumns;
        private readonly List<string> _offerProductColumnsValues;
        private readonly Repository _connection;
        private OfferFileValidator offerImportValidator;
        private readonly decimal _CustomerDefaultFeeValue = ConfigurationManager.AppSettings["CustomerDefaultFee"].TryParse<decimal>() ?? 1;
        private readonly decimal _CurrencyDefaultRate = ConfigurationManager.AppSettings["CurrencyDefaultRate"].TryParse<decimal>() ?? 1;

        public OfferImportService()
        {
            _connection = new Repository();
            offerImportValidator = new OfferFileValidator();
            _offerResourceColumns = OfferImportHelper.GetOfferColumns();
            _offerProductColumnsValues = _offerResourceColumns.Select(x => x.Value).ToList();
        }

        public OfferImport ParseOfferImport(string filePath)
        {
            //get the datasets
            var dataSet = ExcelParseHelper.ParseWorkSheet(filePath);

            offerImportValidator.ValidateFileStructure(dataSet, _offerProductColumnsValues);

            DataTable products;
            DataTable sizes;

            ProcessData(dataSet, out products, out sizes);

            var fileName = Path.GetFileName(filePath);

            var offerImportBatchId = 0;
            var insertedList = InsertBatchItems(fileName, products, sizes, out offerImportBatchId);
            return insertedList;
        }

        public OfferImport InsertBatchItems(string fileName, DataTable products, DataTable productSizes, out int offerImportBatchId)
        {
            var outputIdParam = new SqlParameter("@OfferImportBatchID", SqlDbType.Int) { Direction = ParameterDirection.Output };
            var createdDate = DateTimeOffset.Now;
            var parameters = new SqlParameter[]
            {
               new SqlParameter("@FileName", fileName),
               new SqlParameter("@CreatedBy",1),//todo : principal
               new SqlParameter("@CreatedDate",createdDate),
               new SqlParameter("@StatusID",StatusType.Pending),
               new SqlParameter("@OfferProducts", SqlDbType.Structured)
                {
                    Value = products,
                    TypeName = "OfferProductBatch"
                },
                 new SqlParameter("@OfferProductSizes", SqlDbType.Structured)
                {
                    Value = productSizes,
                    TypeName = "dbo.OfferProductSizeBatch"
                },
               new SqlParameter("@ReturnInsertedList", true),
               outputIdParam
           };

            var data = _connection.LoadData(StoredProcedureNames.OfferImport_Insert, parameters.ToArray());
            offerImportBatchId = outputIdParam.Value != null ? int.Parse(outputIdParam.Value.ToString()) : 0;
            var resultItems = data.ToOfferImportList();

            resultItems = resultItems.Select(x => x.ProcessNumericValues(_CurrencyDefaultRate,_CustomerDefaultFeeValue)).ToList();

            resultItems = resultItems.Select(x => x.ValidateProduct()).ToList();

            var offerImportModel = new OfferImport
            {
                TotalCost = resultItems.Sum(x => x.OfficialTotalPrice),
                TotalUnits = resultItems.Sum(x => x.TotalQuantityValue ?? 0),
                FileName = fileName,
                Id = offerImportBatchId,
                Items = resultItems,
                CreatedDate = createdDate,
                Status = StatusType.Pending
            };
            return offerImportModel;
        }

        private void ProcessData(DataSet dsContent,
                               out DataTable products,
                              out DataTable sizes)
        {
            var dtGenericSizes = dsContent.Tables[0];
            var dtProducts = dsContent.Tables[1];

            var dtGenericSizesColumns = dtGenericSizes.Columns.Cast<DataColumn>().Select(x => x.ColumnName.ToLower().Replace(" ", "")).ToList();
            var dtProductsColumns = dtProducts.Columns.Cast<DataColumn>().Select(x => x.ColumnName.ToLower().Replace(" ", "")).ToList();

            //intersect columns between dtGenericSizes and dtProducts
            var productSizeColumns = dtGenericSizesColumns.Intersect(dtProductsColumns).ToList();
            dtGenericSizes.PrimaryKey = new DataColumn[] { dtGenericSizes.Columns[1] };   //TODO - HARCODED

            //get the datatable with product data , except the sizes columns
            //var offerProductColumnsKeys = _offerResourceColumns.Select(x => x.Key).Distinct().ToList();
            //offerProductColumnsKeys.Add(_indexColumnName);
            //var dtProductsBatch = dtProducts.DefaultView.ToTable(false, offerProductColumnsKeys.ToArray());

            products = GetProductBatchItems(dtProducts, dtGenericSizes, productSizeColumns);
            //get dataTable for sizes
            sizes = GetSizeBatchItems(dtProducts, dtGenericSizes, productSizeColumns);
        }

        private DataTable GetSizeBatchItems(DataTable dtItems, DataTable dtGenericSizes, List<string> productSizeColumns)
        {
            var sizesList = new List<ProductSizeImportModel>();
            for (var i = 0; i < dtItems.Rows.Count; i++)
            {
                var sizeType = dtItems.Rows[i].Field<string>(OfferExcelColumnNames.SizeType);
                if (string.IsNullOrWhiteSpace(sizeType))
                    continue;
                foreach (var columnName in productSizeColumns)
                {
                    var quantity = dtItems.Rows[i][columnName]?.ToString();
                    if (string.IsNullOrWhiteSpace(quantity))
                        continue;
                    var sizeName = dtGenericSizes.Rows.Find(sizeType)?.Field<string>(columnName);
                    if (string.IsNullOrWhiteSpace(sizeName))
                        continue;

                    sizesList.Add(new ProductSizeImportModel
                    {
                        Quantity = quantity,
                        SizeName = sizeName,
                        ProductIndex = i
                    });
                }
            }

            return sizesList.ConvertToDataTable();
        }

        private DataTable GetSizeBatchItems(DataRow row, int rowIndex, DataTable dtGenericSizes, List<string> productSizeColumns)
        {
            var sizesList = new List<ProductSizeImportModel>();

            var sizeType = row.Field<string>(OfferExcelColumnNames.SizeType);
            if (!string.IsNullOrWhiteSpace(sizeType))
            {
                foreach (var columnName in productSizeColumns)
                {
                    if (string.IsNullOrWhiteSpace(row[columnName]?.ToString()))
                        continue;
                    var sizeName = dtGenericSizes.Rows.Find(sizeType)?.Field<string>(columnName);
                    if (string.IsNullOrWhiteSpace(sizeName))
                        continue;

                    sizesList.Add(new ProductSizeImportModel
                    {
                        Quantity = row[columnName].ToString(),
                        SizeName = sizeName,
                        ProductIndex = rowIndex
                    });
                }
            }
            return sizesList.ConvertToDataTable();
        }

        private string GetSizeName(DataTable dtSizeInfo, string columnName, string sizeType)
        {
            DataRow foundRow = dtSizeInfo.Rows.Find(sizeType);
            return foundRow != null ? foundRow.Field<string>(columnName) : null;

        }

        private DataTable GetProductBatchItems(DataTable dtItems, DataTable dtGenericSizes, List<string> productSizeColumns)
        {
            return dtItems.AsEnumerable().Select((dr, index) => new OfferImportModel
            {
                ProductIndex = index,
                Brand = dr.Field<string>(OfferExcelColumnNames.Brand),
                Gender = dr.Field<string>(OfferExcelColumnNames.Gender),
                ModelNumber = dr.Field<string>(OfferExcelColumnNames.ModelNumber),
                ColorCode = dr.Field<string>(OfferExcelColumnNames.ColorCode),
                MaterialCode = dr.Field<string>(OfferExcelColumnNames.MaterialCode),
                SizeType = dr.Field<string>(OfferExcelColumnNames.SizeType),
                CurrencyName = dr.Field<string>(OfferExcelColumnNames.Currency),
                OfferCost = dr.Field<string>(OfferExcelColumnNames.OfferCost),
                Discount = dr.Field<string>(OfferExcelColumnNames.Discount),
                TotalQuantity = dr.Field<string>(OfferExcelColumnNames.TotalQuantity),
                TotalPrice = dr.Field<string>(OfferExcelColumnNames.TotalPrice),
                RetailPrice = dr.Field<string>(OfferExcelColumnNames.RetailPrice),

            }).ToList().ConvertToDataTable();
        }

    }
}
