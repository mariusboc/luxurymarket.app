﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Processors.Helpers;
using LuxuryApp.Core.DataAccess.Repository;

namespace LuxuryApp.Processors.Services
{
    public class ProductService : IProductService
    {
        private readonly Repository _connection;

        //public ProductService(IRepository connection)
        //{
        //    _connection = connection;
        //}

        public ProductService()
        {
            _connection = new Repository();
        }

        public Product GetProductById(int productId)
        {
            if (productId <= 0)
                throw new Exception("Invalid  Id");
            var parameter = new SqlParameter("@ID", productId);

            var reader = _connection.LoadData(StoredProcedureNames.Products_GetByID, parameter);
            return reader.ToProductDetails();

        }

        public Product GetProductBySku(string sku)
        {
            if (string.IsNullOrWhiteSpace(sku))
                throw new Exception("SKU required");
            var parameter = new SqlParameter("@SKU", sku);

            var reader = _connection.LoadData(StoredProcedureNames.Products_GetBySKU, parameter);
            return reader.ToProductDetails();

        }

        public IEnumerable<ProductSearchResult> SearchProduct(ProductSearchItems model)
        {
            var parameters = new SqlParameter[]
            {
              new SqlParameter("@HierarchyID", model.HierarchyId),
              new SqlParameter("@BusinessID", model.BusinessId),
              new SqlParameter("@DivisionID", model.DivisionId),
              new SqlParameter("@DepartmentID", model.DepartmentId),
              new SqlParameter("@CategoryID", model.CategoryId),
              new SqlParameter("@BrandID", model.BrandId),
              new SqlParameter("@Name", model.Name),
              new SqlParameter("@ModelNumber", model.ModelNumber),
              new SqlParameter("@ColorID", model.ColorId),
              new SqlParameter("@CustomerType", null),
        };

            var reader = _connection.LoadData(StoredProcedureNames.Products_Search, parameters);
            return reader.ToProductSearchResult();

        }
    }
}
