﻿using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Processors.Resources.Import;
using System.Collections.Generic;
using System.Linq;
using LuxuryApp.Utils.Helpers;

namespace LuxuryApp.Processors.Validation
{
    public static class OfferImportBatchItemValidator
    {

        public static OfferImportBachItem ValidateProduct(this OfferImportBachItem item)
        {
            if (item == null) return null;

            var errorList = new List<OfferImportError>();

            ValidateDBEntity(item.BrandName, OfferImportBatchItemColumns.Brand, item.BrandId, ref errorList);

            item.ValidateSKU(ref errorList);

            if (errorList.Any())
            {
                item.Errors = errorList;
                return item;
            };

            ValidateDBEntity(item.SizeTypeName, OfferImportBatchItemColumns.SizeType, item.SizeTypeId, ref errorList);

            ValidateDBEntity(item.BusinessName, OfferImportBatchItemColumns.Gender, item.BusinessId, ref errorList);

            ValidateNumericValue(item.RetailPrice, item.RetailPriceValue, null, OfferImportBatchItemColumns.RetailPrice, ref errorList);

            ValidateNumericValue(item.OfferCost, item.OfferCostValue, null, OfferImportBatchItemColumns.OfferCost, ref errorList);

            ValidateNumericValue(item.Discount, item.DiscountValue, item.ActualDiscountValue, OfferImportBatchItemColumns.Discount, ref errorList);

            ValidateNumericValue(item.TotalPrice, item.TotalPriceValue, item.ActualTotalPrice, OfferImportBatchItemColumns.TotalPrice, ref errorList);

            ValidateNumericValue(item.TotalQuantity, item.TotalQuantityValue, item.ActualTotalQuantity, OfferImportBatchItemColumns.TotalQuantity, ref errorList);

            if (item.Currency == CurrencyType.None)
                errorList.Add(CreateError(OfferImportBatchItemColumns.Currency, item.Currency.ToString(), OfferImportBatchItemErrorTypes.NotFound));

            if (!item.Sizes.Any())
                errorList.Add(CreateError(string.Empty, string.Empty, OfferImportBatchItemErrorTypes.NoSizesFound));

            item.Errors = errorList;

            return item;
        }

        public static OfferImportBachItem ProcessNumericValues(this OfferImportBachItem item, decimal rate, decimal customerFee)
        {
            item.Sizes = item.Sizes == null ? item.Sizes : new List<OfferProductSizeImport>();
            item.Sizes.ForEach(x => x.QuantityValue = (x.Quantity ?? string.Empty).TryParse<int>());

            item.TotalPriceValue = ConverterExtension.Round((item.TotalPrice ?? string.Empty).TryParse<decimal>());
            item.DiscountValue = ConverterExtension.Round((item.Discount ?? string.Empty).TryParse<decimal>());
            item.OfferCostValue = ConverterExtension.Round((item.OfferCost ?? string.Empty).TryParse<decimal>());
            item.RetailPriceValue = ConverterExtension.Round((item.RetailPrice ?? string.Empty).TryParse<decimal>());
            item.ActualTotalQuantity = item.Sizes.Sum(x => x.QuantityValue ?? 0);
            item.TotalQuantityValue = (item.TotalQuantity ?? string.Empty).TryParse<int>();
            item.ActualTotalPrice = ConverterExtension.Round(item.OfferCostValue * item.DiscountValue);
            item.ActualDiscountValue = ConverterExtension.Round((item.RetailPriceValue ?? 0) > 0 ? item.OfferCostValue * 100 / item.RetailPriceValue : null);

            rate = (item.Currency != CurrencyType.USD) ? rate : 1;
            item.OfficialTotalPrice = (decimal)ConverterExtension.Round(item.TotalPriceValue ?? 0 * rate * customerFee);

            return item;
        }

        #region Private Methods

        private static string GetErrorMessage(OfferImportBatchItemErrorTypes errorType)
        {
            switch (errorType)
            {
                case OfferImportBatchItemErrorTypes.MandatoryColumn:
                    return OfferImportErrorTypes.MandatoryColumn;
                case OfferImportBatchItemErrorTypes.NotFound:
                    return OfferImportErrorTypes.NotFound;
                case OfferImportBatchItemErrorTypes.InvalidValue:
                    return OfferImportErrorTypes.InvalidValue;
                case OfferImportBatchItemErrorTypes.NoSizesFound:
                    return OfferImportErrorTypes.NoSizesFound;
                    //case OfferImportBatchItemErrorTypes.NotFoundForBrand:
                    //    defaultMessage = OfferImportErrorTypes.NotFound;
                    //    break;
                    //case OfferImportBatchItemErrorTypes.NotFoundForProduct:
                    //    defaultMessage = OfferImportErrorTypes.NotFound;
                    //    break;
            }
            return string.Empty;
        }

        private static OfferImportError CreateError(string columnName, string columnValue, OfferImportBatchItemErrorTypes errorType, string message = null)
        {
            message = string.IsNullOrWhiteSpace(message) ? GetErrorMessage(errorType) : message;

            return new OfferImportError
            {
                ColumnType = columnName,
                ErrorType = errorType,
                Message = string.Format(message, columnName, columnValue)
            };
        }

        private static OfferImportError CreateError(string columnName, string columnValue, string compareValue, OfferImportBatchItemErrorTypes errorType, string message = null)
        {
            message = string.IsNullOrWhiteSpace(message) ? GetErrorMessage(errorType) : message;

            return new OfferImportError
            {
                ColumnType = columnName,
                ErrorType = errorType,
                Message = string.Format(message, columnValue, compareValue)
            };
        }

        private static bool ValidateMandatoryValue(this string columnValue, string columnType, ref List<OfferImportError> errorList)
        {
            if (string.IsNullOrWhiteSpace(columnValue))
            {
                errorList.Add(CreateError(columnType, columnValue, OfferImportBatchItemErrorTypes.MandatoryColumn));
                return false;
            }
            return true;
        }

        private static void ValidateDBEntity(string columnValue, string columnType, int? entityId, ref List<OfferImportError> errorList)
        {
            if (!ValidateMandatoryValue(columnValue, columnType, ref errorList)) return;
            if ((entityId ?? 0) <= 0)
                errorList.Add(CreateError(columnType, columnValue, OfferImportBatchItemErrorTypes.NotFound));
        }

        public static void ValidateNumericValue(string columnValue, decimal? calculatedValue, decimal? compareValue, string columnType, ref List<OfferImportError> errorList)
        {
            if (compareValue != null && calculatedValue != null && calculatedValue != compareValue)
                errorList.Add(CreateError(columnType, columnValue, compareValue.ToString(), OfferImportBatchItemErrorTypes.InvalidValue, string.Empty));
            else if (calculatedValue == null)
                errorList.Add(CreateError(columnType, columnValue, OfferImportBatchItemErrorTypes.InvalidValue));
            // if (!ValidateMandatoryValue(columnValue, columnName, ref errorList)) return;
        }

        private static bool ValidateSKU(this OfferImportBachItem item, ref List<OfferImportError> errorList)
        {
            var isSKUValid =
            ValidateMandatoryValue(item.MaterialCode, OfferImportBatchItemColumns.MaterialCode, ref errorList)
        && ValidateMandatoryValue(item.ColorCode, OfferImportBatchItemColumns.ColorCode, ref errorList)
        && ValidateMandatoryValue(item.ModelNumber, OfferImportBatchItemColumns.ModelNumber, ref errorList);

            if (!isSKUValid) return false;
            if (item.ProductId == null)
            {
                errorList.Add(CreateError(OfferImportBatchItemColumns.SKU, string.Empty, OfferImportBatchItemErrorTypes.NotFound));
                return false;
            }
            return true;
        }

        #endregion
    }
}
