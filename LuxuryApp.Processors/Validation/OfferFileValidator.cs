﻿using LuxuryApp.Core.Infrastructure.Api.Exceptions;
using LuxuryApp.Processors.Resources.Import;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Resources;

namespace LuxuryApp.Processors.Validation
{
    public class OfferFileValidator
    {
        public void ValidateFileStructure(DataSet dataset, List<string> expectedColumns)
        {
            if (dataset?.Tables?.Count != 2)
                throw new CustomValidationException("Invalid file structure. 2 tables are required");

            var dtSizes = dataset.Tables[0];
            if (dtSizes.Columns.Count == 0 || dtSizes.Rows.Count == 0)
                throw new CustomValidationException("Invalid Sizes table - Columns or Rows Missing");


            var dtOffer = dataset.Tables[1];
            if (dtOffer.Columns.Count == 0 || dtOffer.Rows.Count == 0)
                throw new CustomValidationException("Invalid Offer table - Columns or Rows Missing");

            ValidateOfferRequiredColumns(dtOffer, expectedColumns);
        }

        public void ValidateOfferRequiredColumns(DataTable dtOffer, List<string> expectedColumns)
        {
            var columnNames = (from dc in dtOffer.Columns.Cast<DataColumn>()
                               select dc.ColumnName.ToLower().Trim()).ToList();

            var missingColumns = expectedColumns.Where(x => !columnNames.Contains(x)).Select(x => x).Distinct().ToList();

            if (missingColumns.Any())
                throw new CustomValidationException($"Product Offer table - Missing columns: {string.Join(";", missingColumns)}");
        }

    }
}
