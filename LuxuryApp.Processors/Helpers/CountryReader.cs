﻿using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.DataAccess;
using System.Collections.Generic;


namespace LuxuryApp.Processors.Helpers
{
  public static  class CountryReader
    {
        public static List<Country> ToCountryList(this DataReader reader)
        {
            if (reader == null) return null;

            var items = new List<Country>();

            while (reader.Read())
            {
                var item = new Country();
                item.Id = reader.GetInt("CountryID");
                item.CountryName = reader.GetString("CountryName");
                items.Add(item);
            }

            reader.Close();

            return items;
        }

    }
}
