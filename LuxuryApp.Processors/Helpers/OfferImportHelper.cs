﻿using LuxuryApp.Processors.Import;
using LuxuryApp.Processors.Resources.Import;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;

namespace LuxuryApp.Processors.Helpers
{
    public static class OfferImportHelper
    {
        public static Dictionary<string, string> GetOfferColumns()
        {
            Dictionary<string, string> columns = new Dictionary<string, string>();
            var resourceSet = OfferExcelColumnNames.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
            foreach (DictionaryEntry entry in resourceSet)
            {
                columns.Add(entry.Key.ToString().Trim().ToLower(), entry.Value?.ToString().Trim().ToLower());
            }
            return columns;
        }

        public static OfferImportModel ToModel(this DataRow row, int index)
        {
            return new OfferImportModel
            {
                Discount = row.Field<string>(OfferExcelColumnNames.Discount),
                Brand = row.Field<string>(OfferExcelColumnNames.Brand),
                Gender = row.Field<string>(OfferExcelColumnNames.Gender),
                OfferCost = row.Field<string>(OfferExcelColumnNames.OfferCost),
                MaterialCode = row.Field<string>(OfferExcelColumnNames.MaterialCode),
                ColorCode = row.Field<string>(OfferExcelColumnNames.ColorCode),
                ModelNumber = row.Field<string>(OfferExcelColumnNames.ModelNumber),
                SizeType = row.Field<string>(OfferExcelColumnNames.SizeType),
                TotalPrice = row.Field<string>(OfferExcelColumnNames.TotalPrice),
                ProductIndex = index
            };
        }

        public static DataTable ConvertToDataTable<T>(this List<T> data)
        {
            PropertyDescriptorCollection props = null;
            DataTable table = new DataTable();
            if (data != null && data.Count > 0)
            {
                props = TypeDescriptor.GetProperties(data[0]);
                for (int i = 0; i < props.Count; i++)
                {
                    PropertyDescriptor prop = props[i];
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                }
            }
            if (props != null)
            {
                object[] values = new object[props.Count];
                foreach (T item in data)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        values[i] = props[i].GetValue(item) ?? DBNull.Value;
                    }
                    table.Rows.Add(values);
                }
            }
            return table;
        }
    }
}
