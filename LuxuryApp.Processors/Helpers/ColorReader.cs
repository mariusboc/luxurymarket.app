﻿using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.DataAccess;
using System.Collections.Generic;

namespace LuxuryApp.Processors.Helpers
{
    public static class ColorReader
    {
        public static List<Color> ToColorList(this DataReader reader)
        {
            if (reader == null) return null;

            var items = new List<Color>();

            while (reader.Read())
            {
                var item = new Color();
                item.Id = reader.GetInt("ColorID");
                item.StandardColorId = reader.GetInt("StandardColorID");
                item.Description = reader.GetString("Description");
                item.DisplayOrder = reader.GetIntNullable("DisplayOrder");
                item.Code = reader.GetString("ColorCode");
                item.BrandId = reader.GetInt("BrandID");

                items.Add(item);
            }

            reader.Close();

            return items;
        }

    }
}
