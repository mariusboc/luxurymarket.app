﻿using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.DataAccess;
using System.Collections.Generic;
using System.Linq;
using LuxuryApp.Processors.Validation;
using LuxuryApp.Contracts.Enums;

namespace LuxuryApp.Processors.Helpers
{
    public static class OfferImportReader
    {
        public static List<OfferImportBachItem> ToOfferImportList(this DataReader reader)
        {
            if (reader == null) return null;

            var offerImportList = new List<OfferImportBachItem>();

            while (reader.Read())
            {
                var offerImport = new OfferImportBachItem
                {
                    Id = reader.GetInt("OfferImportBatchItemID"),
                    BrandName = reader.GetString("Brand"),
                    BusinessName = reader.GetString("Business"),
                    ModelNumber = reader.GetString("ModelNumber"),
                    ColorCode = reader.GetString("ColorCode"),
                    MaterialCode = reader.GetString("MaterialCode"),
                    SizeTypeName = reader.GetString("SizeType"),
                    OfferCost = reader.GetString("OfferCost"),
                    RetailPrice = reader.GetString("RetailPrice"),
                    Discount = reader.GetString("Discount"),
                    TotalQuantity = reader.GetString("TotalQuantity"),
                    TotalPrice = reader.GetString("TotalPrice"),
                    BrandId = reader.GetIntNullable("BrandID"),
                    BusinessId = reader.GetIntNullable("BusinessID"),
                    ColorId = reader.GetIntNullable("ColorID"),
                    ProductId = reader.GetIntNullable("ProductID"),
                    MaterialId = reader.GetIntNullable("MaterialID"),
                    SizeTypeId = reader.GetIntNullable("SizeTypeID"),
                    HierarchyId = reader.GetIntNullable("HierarchyID"),
                    Currency = (CurrencyType)(reader.GetIntNullable("CurrencyID") ?? 0),

                    //product
                    //Dimensions = reader.GetString("Dimensions"),
                    //MainImageId = reader.GetIntNullable("MainImageID"),
                    MainImageName = reader.GetString("MainImageName"),
                    //OriginId = reader.GetIntNullable("OriginID"),
                    //OriginName = reader.GetString("OriginName"),
                    //SeasonId = reader.GetIntNullable("SeasonID"),
                    //SeasonName = reader.GetString("SeasonName"),
                    Summary = reader.GetString("Summary"),
                    ProductName = reader.GetString("ProductName"),
                    CategoryName = reader.GetString("CategoryName")
                };
                offerImportList.Add(offerImport);
            }

            var sizes = new List<OfferProductSizeImport>();

            if (reader.NextResult())
            {
                while (reader.Read())
                {
                    var sizeItem = new OfferProductSizeImport
                    {
                        OfferImportBatchItemId = reader.GetInt("OfferImportBatchItemID"),
                        OfferImportBatchSizeItemId = reader.GetInt("OfferImportBatchSizeItemID"),
                        Quantity = reader.GetString("Quantity"),
                        Size = reader.GetString("Size"),
                        SizeId = reader.GetIntNullable("SizeID")
                    };
                    sizes.Add(sizeItem);
                }
            }

            reader.Close();

            offerImportList.ForEach(x => x.Sizes = sizes.Where(y => y.OfferImportBatchItemId == x.Id).ToList());
            return offerImportList;
        }
    }
}
