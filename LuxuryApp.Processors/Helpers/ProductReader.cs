﻿using LuxuryApp.Contracts.Enums;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Core.DataAccess;
using System.Collections.Generic;

namespace LuxuryApp.Processors.Helpers
{
    public static class ProductReader
    {
        public static Product ToProductDetails(this DataReader reader)
        {
            //var reader = new DataReader { Reader = dataReader };

            if (reader == null) return null;

            var product = new Product();

            while (reader.Read())
            {

                product.Id = reader.GetInt("ID");
                product.Name = reader.GetString("Name");
                product.Summary = reader.GetString("Summary");
                product.ModelNumber = reader.GetString("ModelNumber");
                product.SKU = reader.GetString("SKU");
                product.MainImageId = reader.GetIntNullable("MainImageID");
                product.MainImageName = reader.GetString("MainImageName");

                product.ParentId = reader.GetIntNullable("ParentID");
                product.HierarchyId = reader.GetInt("HierarchyID");
                product.Variant = reader.GetString("Variant");
                product.RetailCurrency = (CurrencyType)(reader.GetIntNullable("RetailCurrency") ?? 0);
                product.CostCurrency = (CurrencyType)(reader.GetIntNullable("CostCurrency") ?? 0);
                product.RetailPrice = reader.GetDoubleNullable("RetailPrice");
                product.WholeSaleCost = reader.GetDoubleNullable("WholeSaleCost");

                product.Brand = new Brand
                {
                    Id = reader.GetInt("BrandID"),
                    Name = reader.GetString("BrandName"),
                    Description = reader.GetString("BrandDescription"),
                    StandardBrand = new StandardBrandModel
                    {
                        StandardId = reader.GetInt("StandardBrandID"),
                        Name = reader.GetString("StandardBrandName")
                    }
                };

                product.Color = new Color
                {
                    Id = reader.GetInt("ColorID"),
                    Code = reader.GetString("ColorCode"),
                    Description = reader.GetString("ColorDescription"),
                    StandardColor = new StandardColorsModel
                    {
                        StandardId = reader.GetInt("StandardColorID"),
                        Name = reader.GetString("StandardColor"),
                        RGB = reader.GetString("StandardRGB")
                    }
                };
                product.Material = new Material
                {
                    Id = reader.GetInt("MaterialID"),
                    Code = reader.GetString("MaterialCode"),
                    Description = reader.GetString("MaterialDescription"),
                    StandardMaterial = new StandardMaterialModel
                    {
                        StandardId = reader.GetInt("StandardMaterialID"),
                        Name = reader.GetString("StandardMaterial")
                    }
                };
                product.Origin = new Origin
                {
                    Id = reader.GetInt("OriginID"),
                    Name = reader.GetString("OriginName"),
                    Description = reader.GetString("OriginDescription"),
                    StandardOrigin = new StandardOriginModel
                    {
                        StandardId = reader.GetInt("StandardOriginID"),
                        Name = reader.GetString("StandardOriginName")
                    }
                };

                product.Season = new Season
                {
                    Id = reader.GetInt("SeasonID"),
                    Name = reader.GetString("SeasonCode"),
                    Description = reader.GetString("SeasonDescription"),
                    Year = reader.GetInt("SeasonYear"),
                    StandardSeason = new StandardSeasonModel
                    {
                        StandardId = reader.GetInt("StandardSeasonID"),
                        Name = reader.GetString("StandardSeasonName")
                    }
                };
                product.SizeType = new SizeType
                {
                    Id = reader.GetInt("SizeTypeID"),
                    Name = reader.GetString("SizeTypesName"),
                    Description = reader.GetString("SizeTypesDescription")
                };

            }

            if (reader.NextResult())
            {
                product.Images = GetEntityImages(reader);
            }

            if (reader.NextResult())
            {
                product.EntityDescriptions = GetEntityDescriptions(reader);
            }

            if (reader.NextResult())
            {
                product.Sizes = GetSizes(reader);
            }

            reader.Close();

            return product;
        }

        public static List<EntityImage> GetEntityImages(DataReader reader)
        {
            if (reader == null) return null;

            var images = new List<EntityImage>();

            while (reader.Read())
            {
                images.Add(new EntityImage
                {
                    Id = reader.GetInt("ID"),
                    EntityId = reader.GetInt("EntityID"),
                    EntityType = (EntityType)reader.GetInt("EntityTypeID"),
                    Name = reader.GetString("Name"),
                    Link = reader.GetString("Link"),
                    DisplayOrder = reader.GetInt("DisplayOrder")
                });
            }

            return images;
        }

        public static List<EntityDescription> GetEntityDescriptions(DataReader reader)
        {
            if (reader == null) return null;

            var list = new List<EntityDescription>();

            while (reader.Read())
            {
                list.Add(new EntityDescription
                {
                    Id = reader.GetInt("ID"),
                    EntityId = reader.GetInt("EntityID"),
                    EntityType = (EntityType)reader.GetInt("EntityTypeID"),
                    Description = reader.GetString("Description"),
                    Notes = reader.GetString("Notes"),
                    Dimensions = reader.GetString("Dimensions")
                });
            }

            return list;
        }

        public static List<Size> GetSizes(DataReader reader)
        {
            if (reader == null) return null;

            var list = new List<Size>();

            while (reader.Read())
            {
                list.Add(new Size
                {
                    Id = reader.GetInt("ID"),
                    Name = reader.GetString("Name"),
                    SizeType = new SizeType
                    {
                        Id = reader.GetInt("SizeTypesID"),
                        Name = reader.GetString("SizeTypesName"),
                        Description = reader.GetString("SizeTypesDescription"),
                    }
                });
            }
            return list;
        }

        public static IEnumerable<ProductSearchResult> ToProductSearchResult(this DataReader reader)
        {
            //  var reader = new DataReader { Reader = dataReader };

            if (reader == null) return null;

            var products = new List<ProductSearchResult>();

            while (reader.Read())
            {
                products.Add(
                    new ProductSearchResult
                    {
                        Id = reader.GetInt("ID"),
                        Name = reader.GetString("Name"),
                        Summary = reader.GetString("Summary"),
                        ModelNumber = reader.GetString("ModelNumber"),
                        SKU = reader.GetString("SKU"),
                        MainImageId = reader.GetIntNullable("MainImageID"),
                        MainImageName = reader.GetString("MainImageName"),

                        HierarchyId = reader.GetInt("HierarchyID"),
                        Variant = reader.GetString("Variant"),
                        RetailCurrency = (CurrencyType)(reader.GetIntNullable("RetailCurrency") ?? 0),
                        CostCurrency = (CurrencyType)(reader.GetIntNullable("CostCurrency") ?? 0),
                        RetailPrice = reader.GetDoubleNullable("RetailPrice"),
                        WholeSaleCost = reader.GetDoubleNullable("WholeSaleCost"),

                        ColorId = reader.GetInt("ColorID"),
                        ColorCode = reader.GetString("ColorCode"),
                        StandardColorName = reader.GetString("StandardColorName"),

                        BrandId = reader.GetInt("BrandID"),
                        BrandName = reader.GetString("BrandName"),
                        StandardBrandName = reader.GetString("StandardBrandName"),
                    });
            }

            reader.Close();

            return products;
        }

        public static List<State> ToStateModel(this DataReader reader)
        {
            if (reader == null) return null;

            var items = new List<State>();

            while (reader.Read())
            {
                var item = new State();
                item.StateId = reader.GetInt("StateID");
                item.StateName = reader.GetString("StateName");
                item.CountryId = reader.GetInt("CountryID");

                items.Add(item);
            }

            reader.Close();

            return items;
        }

       }
}
