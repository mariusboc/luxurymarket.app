﻿using System;

namespace LuxuryApp.Processors.Import
{
    [Serializable]
    public class OfferImportModel
    {
        public string Brand { get; set; }

        public string Gender { get; set; }

        public string ModelNumber { get; set; }

        public string ColorCode { get; set; }

        public string MaterialCode { get; set; }

        public string SizeType { get; set; }

        public string CurrencyName { get; set; }

        public string OfferCost { get; set; }

        public string RetailPrice { get; set; }

        public string Discount { get; set; }

        public string TotalQuantity { get; set; }

        public string TotalPrice { get; set; }

        public int ProductIndex { get; set; }
    }

    public class ProductSizeImportModel
    {
        public int ProductIndex { get; set; }

        public string SizeName { get; set; }

        public string Quantity { get; set; }
    }
}