﻿
namespace LuxuryApp.Processors
{
    public static class StoredProcedureNames
    {
        public static string RegisterRequest_Insert = "App.RegistrationRequest_Insert";

        public static string Products_GetByID = "App.usp_ProductGetByID";
        public static string Products_GetBySKU = "App.usp_ProductGetBySku";
        public static string Products_Search = "APP.usp_ProductSearch";

        public static string OfferImport_Insert = "usp_OfferImport_Insert";

        public static string Countries_Search = "Countries_GetByID";
        public static string States_Search = "States_Search";
        public static string CurrencyGetByID = "usp_CurrencyGetByID";

        public static string BrandsGetByID = "usp_BrandsGetByID";
        public static string ColorsSearch = "usp_ColorsSearch";
        public static string BrandsSearch = "usp_BrandsSearch";
        public static string MaterialSearch = "usp_MaterialSearch";
    }
}
