﻿namespace LuxuryApp.Contracts.Enums
{
    public enum OfferImportBatchItemErrorTypes
    {
        None,

        MandatoryColumn,

        InvalidValue,

        NotFound,

        //NotFoundForBrand,

        //NotFoundForProduct,

        NoSizesFound
    }
}
