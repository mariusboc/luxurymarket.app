﻿
namespace LuxuryApp.Contracts.Enums
{
    public enum CompanyType
    {
        None,

        Seller,

        Buyer,

        Both
    }
}
