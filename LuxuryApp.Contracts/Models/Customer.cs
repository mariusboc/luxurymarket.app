﻿using System.Collections.Generic;

namespace LuxuryApp.Contracts.Models
{
    public class Customer
    {
        public string Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public IEnumerable<Company> Companies { get; set; }
    }
}
