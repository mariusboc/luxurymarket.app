﻿using LuxuryApp.Contracts.Enums;

namespace LuxuryApp.Contracts.Models
{
    public class ProductSearchResult
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string ModelNumber { get; set; }

        public string Variant { get; set; }

        public string SKU { get; set; }

        public string Summary { get; set; }

        public int? MainImageId { get; set; }

        public string MainImageName { get; set; }

        public int HierarchyId { get; set; }

        public CurrencyType RetailCurrency { get; set; }

        public double? RetailPrice { get; set; }

        public CurrencyType CostCurrency { get; set; }

        public double? WholeSaleCost { get; set; }

        public int ColorId { get; set; }

        public string ColorCode { get; set; }

        public string StandardColorName { get; set; }

        public int BrandId { get; set; }

        public string BrandName { get; set; }

        public string StandardBrandName { get; set; }
    }
}