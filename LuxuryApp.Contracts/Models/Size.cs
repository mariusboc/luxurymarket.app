﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models
{
    public class Size
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public SizeType SizeType { get; set; }
    }
}
