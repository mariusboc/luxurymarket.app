﻿using LuxuryApp.Contracts.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LuxuryApp.Contracts.Models
{
    public class OfferImport
    {
        public int Id { get; set; }

        public string FileName { get; set; }

        public StatusType Status { get; set; }

        public bool TakeAll { get; set; }

        public DateTimeOffset StartDate { get; set; }

        public DateTimeOffset EndDate { get; set; }

        public DateTimeOffset CreatedDate { get; set; }

        public IEnumerable<OfferImportBachItem> Items { get; set; }

        public decimal TotalSuccessfulItems
        {
            get
            {
                return Items.Any() ? Items.Where(x => !x.HasErrors).Count() : 0;
            }
        }

        public decimal TotalErrorItems
        {
            get
            {
                return Items.Any() ? Items.Where(x => x.HasErrors).Count() : 0;
            }
        }

        public int TotalUnits { get; set; }

        public decimal TotalCost { get; set; }
    }
}
