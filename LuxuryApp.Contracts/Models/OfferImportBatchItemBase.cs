﻿using LuxuryApp.Contracts.Enums;
using System.Collections.Generic;
using System.Linq;

namespace LuxuryApp.Contracts.Models
{
    public class OfferImportBatchItemBase
    {
        public int Id { get; set; }

        public string BrandName { get; set; }

        public string BusinessName { get; set; }

        public string ModelNumber { get; set; }

        public string ColorCode { get; set; }

        public string MaterialCode { get; set; }

        public string SizeTypeName { get; set; }

        public string OfferCost { get; set; }

        public string RetailPrice { get; set; }

        public string Discount { get; set; }

        public string TotalQuantity { get; set; }

        public string TotalPrice { get; set; }

        public int? BrandId { get; set; }

        public int? BusinessId { get; set; }

        public int? ProductId { get; set; }

        public int? HierarchyId { get; set; }

        public CurrencyType Currency { get; set; }

        public string SKU { get; set; }

        public int? ColorId { get; set; }

        public int? MaterialId { get; set; }

        public int? SizeTypeId { get; set; }

        public List<OfferProductSizeImport> Sizes { get; set; }

        //product values

        public string CategoryName { get; set; }

        public string ProductName { get; set; }

        public string Summary { get; set; }

        public string MainImageName { get; set; }
    }
}
