﻿
namespace LuxuryApp.Contracts.Models
{
    public class OfferProductSizeImport
    {
        public int OfferImportBatchSizeItemId { get; set; }

        public int OfferImportBatchItemId { get; set; }

        public string Size { get; set; }

        public int? SizeId { get; set; }

        public string Quantity { get; set; }

        public int? QuantityValue { get; set; }
    }
}
