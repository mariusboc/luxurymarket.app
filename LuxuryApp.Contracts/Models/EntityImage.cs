﻿using LuxuryApp.Contracts.Enums;

namespace LuxuryApp.Contracts.Models
{
    public class EntityImage
    {
        public int Id { get; set; }

        public int EntityId { get; set; }

        public string Name { get; set; }

        public string Link { get; set; }

        public int DisplayOrder { get; set; }

        public EntityType EntityType{ get; set; }
    }
}
