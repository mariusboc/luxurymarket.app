﻿using LuxuryApp.Contracts.Enums;
using System.Collections.Generic;

namespace LuxuryApp.Contracts.Models
{
    public class ProductBaseModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string ModelNumber { get; set; }

        public string Variant { get; set; }

        public string SKU { get; set; }

        public string Description { get; set; }

        public string Summary { get; set; }

        public int? MainImageId { get; set; }

        public string MainImageName { get; set; }

        public int? ParentId { get; set; }

        public int HierarchyId { get; set; }

        public CurrencyType RetailCurrency { get; set; }

        public double? RetailPrice { get; set; }

        public CurrencyType CostCurrency { get; set; }

        public double? WholeSaleCost { get; set; }
    }

    public class Product : ProductBaseModel
    {
        public Brand Brand { get; set; }

        public Business Business { get; set; }

        public DivisionModel Division { get; set; }

        public Category Category { get; set; }

        public Color Color { get; set; }

        public Material Material { get; set; }

        public Origin Origin { get; set; }

        public SizeType SizeType { get; set; }

        public List<Size> Sizes { get; set; }

        public Season Season { get; set; }

        public List<EntityImage> Images { get; set; }

        public List<EntityDescription> EntityDescriptions { get; set; }
    }
}