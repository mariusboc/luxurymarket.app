﻿
using LuxuryApp.Contracts.Enums;

namespace LuxuryApp.Contracts.Models
{
    public class OfferImportError
    {
       // public int OfferImportBatchItemID { get; set; }

        public string ColumnType { get; set; }

        public OfferImportBatchItemErrorTypes ErrorType { get; set; }

        public string Message { get; set; }
    }
}
