﻿using System.Collections.Generic;
using System.Linq;

namespace LuxuryApp.Contracts.Models
{
    public class OfferImportBachItem : OfferImportBatchItemBase
    {
        public decimal? OfferCostValue { get; set; }

        public decimal? RetailPriceValue { get; set; }

        public decimal? DiscountValue { get; set; }

        public decimal? ActualDiscountValue { get; set; }

        public decimal? TotalPriceValue { get; set; }

        public int ActualTotalQuantity { get; set; }

        public int? TotalQuantityValue { get; set; }

        public decimal? ActualTotalPrice { get; set; }

        public List<OfferImportError> Errors { get; set; }

        public decimal OfficialTotalPrice { get; set; }
        public bool HasErrors
        {
            get
            {
                return Errors != null && Errors.Any();
            }
        }
    }
}
