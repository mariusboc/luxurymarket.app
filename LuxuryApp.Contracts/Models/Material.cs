﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Contracts.Models
{
    public class Material : Attribute
    {
        public int BrandId { get; set; }

        public int StandardMaterialId { get; set; }

        public StandardMaterialModel StandardMaterial { get; set; }
    }
}
