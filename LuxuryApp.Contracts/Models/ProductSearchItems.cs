﻿using LuxuryApp.Contracts.Enums;

namespace LuxuryApp.Contracts.Models
{
    public class ProductSearchItems
    {
        public int? HierarchyId { get; set; }

        public int? BusinessId { get; set; }

        public int? DivisionId { get; set; }

        public int? DepartmentId { get; set; }

        public int? CategoryId { get; set; }

        public int? BrandId { get; set; }

       public string Name { get; set; }

        public string ModelNumber { get; set; }

        public int? ColorId { get; set; }

        //public CustomerType @CustomerType { get; set; }
    }
}