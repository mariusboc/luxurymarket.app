﻿using System;
using System.Runtime.Serialization;

namespace LuxuryApp.Contracts.Exceptions
{
    /// <summary>
    /// Thrown when we attempt to get/update/delete and entity that cannot be retreived 
    /// </summary>
    [Serializable]
    public class ResourceNotFoundException : Exception
    {
        public Guid Guid { get; set; }

        public ResourceNotFoundException()
        {
        }

        public ResourceNotFoundException(string message)
            : base(message)
        {
        }

        public ResourceNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected ResourceNotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}