﻿using LuxuryApp.Contracts.Models;

namespace LuxuryApp.Contracts.Services
{
    public interface IRegistrationRequestService
    {
        int CreateRequest(RegistrationRequest model);
    }
}
