﻿using System.Collections.Generic;
using LuxuryApp.Contracts.Models;

namespace LuxuryApp.Contracts.Services
{
    public interface ICustomerService
    {
        int CreateCustomer(Customer modelRequest);

        int AddNewCustomersToCompany(IEnumerable<Customer> customers, int companyId);

        int AddNewCustomersToCompany(Customer customers, int companyId);
    }
}
