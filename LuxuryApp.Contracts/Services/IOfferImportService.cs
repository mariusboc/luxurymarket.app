﻿
using LuxuryApp.Contracts.Models;
using System.Collections.Generic;

namespace LuxuryApp.Contracts.Services
{
    public interface IOfferImportService
    {
        OfferImport ParseOfferImport(string filePath);
    }
}
