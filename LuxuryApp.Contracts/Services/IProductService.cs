﻿using LuxuryApp.Contracts.Models;
using System.Collections.Generic;

namespace LuxuryApp.Contracts.Services
{
    public interface IProductService
    {
        Product GetProductById(int productId);
    
        Product GetProductBySku(string sku);

        IEnumerable<ProductSearchResult> SearchProduct(ProductSearchItems model);
     }
}
