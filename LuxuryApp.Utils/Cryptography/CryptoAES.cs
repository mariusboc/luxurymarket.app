﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace LuxuryApp.Utils.Cryptography
{
    public class CryptoAES
    {
        private const string StringKey = "LuxuryP@55w0rd!_F0r5t0r1ngS3cur3s";
        //The initialization vector should random and different for each encryption. Similar to salt.
        //But we loose the posibility to search for duplicates in the DB.
        private const string StringVector = "In1t1@liz@tionV_";
        private static readonly byte[] key = Encoding.UTF8.GetBytes(StringKey);
        private static readonly byte[] vector = Encoding.UTF8.GetBytes(StringVector);

        private ICryptoTransform encryptor, decryptor;
        private UTF8Encoding encoder;

        public CryptoAES()
        {
            var rm = new RijndaelManaged();
            encryptor = rm.CreateEncryptor(key, vector);
            decryptor = rm.CreateDecryptor(key, vector);
            encoder = new UTF8Encoding();
        }

        public string Encrypt(string unencrypted)
        {
            return Convert.ToBase64String(Encrypt(encoder.GetBytes(unencrypted)));
        }

        public string Decrypt(string encrypted)
        {
            return encoder.GetString(Decrypt(Convert.FromBase64String(encrypted)));
        }

        public byte[] Encrypt(byte[] buffer)
        {
            return Transform(buffer, encryptor);
        }

        public byte[] Decrypt(byte[] buffer)
        {
            return Transform(buffer, decryptor);
        }

        protected byte[] Transform(byte[] buffer, ICryptoTransform transform)
        {
            var stream = new MemoryStream();
            using (var cs = new CryptoStream(stream, transform, CryptoStreamMode.Write))
            {
                cs.Write(buffer, 0, buffer.Length);
            }
            return stream.ToArray();
        }
    }
}
