﻿using System.Collections.Generic;

namespace LuxuryApp.Utils.Models
{
    public abstract class BaseSheetModel
    {
        public abstract BaseSheetModel MapCellArray(Dictionary<string, string> cellArray, Dictionary<string, string> columnDictionary);

        public int RowIndex { get; set; }
    }
}
