﻿using OfficeOpenXml;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Data;
using System;

namespace LuxuryApp.Utils.Helpers
{
    public static class ExcelParseHelper
    {
        public static DataTable ParseWorkSheet(string path, int startRow, bool hasHeader)
        {
            using (var package = new ExcelPackage())
            {
                using (var stream = new FileInfo(path).OpenRead())
                {
                    package.Load(stream);
                }

                var workSheet = package.Workbook.Worksheets.First();

                var dt = GetDataTable(workSheet, startRow, hasHeader);

                return dt;
            }
        }

        public static DataSet ParseWorkSheet(string path)
        {
            using (var package = new ExcelPackage())
            {
                using (var stream = new FileInfo(path).OpenRead())
                {
                    package.Load(stream);
                }

                var workSheet = package.Workbook.Worksheets.First();

                var dt = GetDataSet(workSheet);

                return dt;
            }
        }

        public static DataTable ClearEmptyColumns(DataTable dtExcel)
        {
            if (dtExcel == null) return dtExcel;
            string[] columnNames = (from dc in dtExcel.Columns.Cast<DataColumn>()
                                    where dc.ColumnName.ToLower().StartsWith("column")
                                    select dc.ColumnName).ToArray();
            foreach (var columnName in columnNames)
            {
                if (dtExcel.Rows.OfType<DataRow>().All(r => r.IsNull(columnName)))
                    dtExcel.Columns.Remove(columnName);
            }
            return dtExcel;
        }

        public static bool HasNull(this DataTable table)
        {
            foreach (DataColumn column in table.Columns)
            {
                if (table.Rows.OfType<DataRow>().Any(r => r.IsNull(column)))
                    return true;
            }

            return false;
        }

        public static DataTable GetDataTable(ExcelWorksheet workSheet, int startRow, bool hasHeader)
        {
            if (workSheet == null) return null;

            var dtExcel = new DataTable();
            if (hasHeader)
            {
                while (workSheet.Cells[startRow, 1, startRow, workSheet.Dimension.End.Column].IsEmptyRow())
                {
                    startRow++;
                }
                var headerColumns = GetColumnHeaders(workSheet, startRow);
                foreach (var columnName in headerColumns)
                {
                    dtExcel.Columns.Add(columnName);
                }
                startRow++;
            }

            for (int rowNum = startRow; rowNum <= workSheet.Dimension.End.Row; rowNum++)
            {
                var wsRow = workSheet.Cells[rowNum, 1, rowNum, workSheet.Dimension.End.Column];
                if (wsRow.IsEmptyRow())
                    continue;

                var row = dtExcel.NewRow();
                foreach (var cell in wsRow)
                {
                    row[cell.Start.Column - 1] = cell.Value?.ToString().Trim();
                }
                dtExcel.Rows.Add(row);
            }

            dtExcel = ClearEmptyColumns(dtExcel);
            return dtExcel;
        }

        /// <summary>
        /// Get all tables from worksheet
        /// All tables are separated by an empty row
        /// </summary>
        /// <param name="workSheet"></param>
        /// <returns></returns>
        public static DataSet GetDataSet(ExcelWorksheet workSheet)
        {
            if (workSheet == null) return null;

            var dataset = new DataSet();
            DataTable dtExcel = null;
            for (int rowNum = 1; rowNum <= workSheet.Dimension.End.Row; rowNum++)
            {
                var wsRow = workSheet.Cells[rowNum, 1, rowNum, workSheet.Dimension.End.Column];
                if (wsRow.IsEmptyRow())
                {
                    if (dtExcel?.Rows.Count > 0)
                    {
                        dtExcel = ClearEmptyColumns(dtExcel);
                        if (dtExcel?.Rows.Count > 0)
                            dataset.Tables.Add(dtExcel);
                        dtExcel = null;
                    }
                    continue;
                }
                if (dtExcel == null)
                {
                    dtExcel = new DataTable();
                    var headerColumns = GetColumnHeaders(workSheet, rowNum);
                    foreach (var columnName in headerColumns)
                    {
                        dtExcel.Columns.Add(columnName);
                    }
                }
                else
                {
                    var dtRow = dtExcel.NewRow();
                    foreach (var cell in wsRow)
                    {
                        dtRow[cell.Start.Column - 1] = cell.Value?.ToString().Trim();
                    }
                    dtExcel.Rows.Add(dtRow);
                }

            }

            return dataset;
        }

        public static List<string> GetColumnHeaders(ExcelWorksheet workSheet, int row)
        {
            var values = workSheet.Cells[row, 1, row, workSheet.Dimension.End.Column].Select(x => x.Text?.Trim().ToLower()).ToList();
            return values;
        }

        //public static List<T> ParseWorkSheet<T>(this FileInfo file, int startRow, bool hasHeader) where T : BaseSheetModel
        //{
        //    var ws = GetWorksheetFromFile(file);

        //    var dt = GetDataTable(ws, startRow, hasHeader);

        //    //var elem = (T)Activator.CreateInstance<T>().MapCellArray(cellArray, columnDictionary);
        //    //elem.RowIndex = (int)row.RowIndex.Value;
        //    //result.Add(elem);

        //    return result;
        //}
    }

    public static class ExcelExtensions
    {
        public static bool IsEmptyRow(this ExcelRange row)
        {
            return row == null || row.All(x => string.IsNullOrWhiteSpace(x.Value?.ToString()));
        }

    }
}
