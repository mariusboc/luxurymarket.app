﻿using System.Collections;
using System.Collections.Generic;
using System.Resources;


namespace LuxuryApp.Utils.Helpers
{
    public static class ResourcesHelper
    {
        public static Dictionary<string, string> GetResourceValues(string resourcePath)
        {
            using (var rsxr = new ResXResourceReader(resourcePath))
            {
                var columns = new Dictionary<string, string>();

                foreach (DictionaryEntry d in rsxr)
                {
                    columns.Add(d.Key.ToString(), d.Value.ToString().Trim().ToLower());
                }

                return columns;
            }
        }
    }
}
