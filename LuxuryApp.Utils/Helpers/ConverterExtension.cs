﻿using System;

namespace LuxuryApp.Utils.Helpers
{
    public static class ConverterExtension
    {
        public static Nullable<T> TryParse<T>(this String str) where T : struct
        {
            try
            {
                T parsedValue = (T)Convert.ChangeType(str, typeof(T));
                return parsedValue;
            }
            catch { return null; }
        }

        public static decimal? Round(decimal? value)
        {
            return value != null ? (decimal?)decimal.Round((decimal)value, 2, MidpointRounding.AwayFromZero) : null;
        }
    }
}
