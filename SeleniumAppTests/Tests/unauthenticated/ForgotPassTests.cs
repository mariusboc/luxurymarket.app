﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SeleniumAppTests.Tests.unauthenticated
{
    [TestClass]
    public class ForgotPassTests : BaseSeleniumTest
    {

        #region Positive Tests
        /// <summary>
        /// Resets the password, verifies the message at the end of the reset flow, goes back to the login page and verifies that the login button is present
        /// </summary>
        [TestMethod]
        public void ResetPassword()
        {
            var loginPage = NavigateToApplication();
            var forgotpage = loginPage.forgotPassClick();

            forgotpage.setResetEmail("buyer1@mejix.com");
            forgotpage.changePasswordClick();
            forgotpage.goToLoginClick();  
        }
        
        #endregion



    }
}
