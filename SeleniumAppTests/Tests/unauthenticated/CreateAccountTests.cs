﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SeleniumAppTests.PageObjects.authentication;
using System.Threading;

namespace SeleniumAppTests.Tests.unauthenticated
{
    [TestClass]
    public class CreateAccountTests : BaseSeleniumTest
    {
        #region Positive Tests
        [TestMethod]
        public void CreateBuyer()
        {

            var loginPage = NavigateToApplication();
            var createaccPage = loginPage.newAccountClick();

            createaccPage.SetAccountType(AccountTypes.BUYER);
            createaccPage.SetContactName("AutomationBuyer" + DateTime.Now.ToString(" dd.MM.yyyy_HH:mm:ss"));
            createaccPage.SetCompany("AutomationBuyer" + DateTime.Now.ToString(" dd.MM.yyyy_HH:mm:ss"));
            createaccPage.SetAddress("Automation Address NY");
            createaccPage.SetCity("Automation City NY");
            Thread.Sleep(2000);
            createaccPage.SetContryDropdown("USA");
            Thread.Sleep(2000);
            createaccPage.SetStateDropdown("New York");
            createaccPage.SetZipCode("10018");
            createaccPage.SetRetail("Automatio retail ID");
            createaccPage.SetPhone("555");
            createaccPage.SetWebsite("www.automation.com");
            createaccPage.SetEmail("automation@mejix.com");
            createaccPage.SetConfirmEmail("automation@mejix.com");
            createaccPage.SetDescription("Test description");
            createaccPage.TermsCheckbox();
            createaccPage.SubmitClick();
            Thread.Sleep(2000);                      
        }

        #endregion


        #region Negative Tests





        #endregion




    }
}
