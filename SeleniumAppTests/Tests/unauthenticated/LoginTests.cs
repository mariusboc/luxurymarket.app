﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using OpenQA.Selenium;
using System.Threading;

namespace SeleniumAppTests.Tests.unauthenticated
{
    [TestClass]

    public class LoginTests : BaseSeleniumTest
    {
        #region Positive Tests
        /// <summary>
        /// Logs in, verifies that the main banner is present than logs out
        /// </summary>
        [TestMethod]
        public void LoginAndOut()
        {
            var loginpage = NavigateToApplication();
            loginpage.setUsername("automation@mejix.com");
            loginpage.setPassword("222222");
            var site = loginpage.loginClick();
            site.Header.UserMenu.SignOut();
        }

        #endregion

        #region Negative Tests

        [TestMethod]
        public void IncorrectPassword()
        {
            var loginpage = NavigateToApplication();
            loginpage.setUsername("automation@mejix.com");
            loginpage.setPassword("q");
            var site = loginpage.loginClick();
        }

        #endregion
    }
}
