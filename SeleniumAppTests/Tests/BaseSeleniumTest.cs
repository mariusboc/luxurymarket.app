﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SeleniumAppTests.Helpers;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using SeleniumAppTests.PageObjects.authentication;

namespace SeleniumAppTests.Tests
{
    /// <summary>
    /// Summary description for BaseSeleniumTest
    /// </summary>
    [TestClass]
    public class BaseSeleniumTest
    {
        #region Web Elements Identifiers

        private String _loginFormID= "loginform";

        #endregion

        public BaseSeleniumTest()
        {
            WebDriverHelper.Init();
            
        }

        [TestCleanup()]
        public void Cleanup()
        {
            WebDriverHelper.Driver.Close();
        }

        protected LoginPage NavigateToApplication()
        {
            WebDriverHelper.Driver.Navigate().GoToUrl(AppConfig.AppUrl);

            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_loginFormID)));

            return new LoginPage(WebDriverHelper.Driver);
        }


    }
}
