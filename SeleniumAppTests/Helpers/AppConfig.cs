﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAppTests.Helpers
{
    public class AppConfig
    {
        public static String AppUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["appUrl"];
            }
        }

        /// <summary>
        /// Time for page to load. Value is in seconds.
        /// </summary>
        public static int WaitForPageLoad
        {
            get
            {
                int pageLoadTime = -1;
                int.TryParse(ConfigurationManager.AppSettings["waitForPageLoad"], out pageLoadTime);

                return pageLoadTime == -1 ? 10 : pageLoadTime;
            }
        }
    }
}
