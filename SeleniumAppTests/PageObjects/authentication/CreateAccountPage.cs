﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace SeleniumAppTests.PageObjects.authentication
{
    public enum AccountTypes
    {
        BUYER, SELLER
    }
    public class CreateAccountPage : BasePage
    {

        #region Web Elements identifiers

        private String _registrationFormID = ("registrationform");
        private String _createBuyerID = ("buyer");
        private String _createSellerID = ("seller");
        private String _contactNameID = ("contactname");
        private String _companyNameID = ("companyname");
        private String _streetAddressID = ("streetaddrs");
        private String _cityID = ("city");
        private String _countryID = ("country");
        private String _stateID = ("state");
        private String _zipcodeID = ("zip");
        private String _retailID = ("retailID");
        private String _phoneNumberID = ("phonenumber");
        private String _companywebsiteID = ("companywebsite");
        private String _contactEmailID = ("contactemail");
        private String _confirmEmailID = ("confirmcontactemail");
        private String _descriptionID = ("businessdescription");
        private String _termConditionsCheckboxID = ("termsconditionscheckbox");
        private String _termsAndConditionsPageID = ("termsandconditionspage");
        private String _submitRequestID = ("submit");
        private String _goLoginCreateAccID = ("gotologincreateacc");
        private String _confirmationMsgID = ("messageform");
        private String _goToLoginAfterCreateAccID = ("gotologin");

        #endregion

        #region Web Elements

        private IWebElement registrationPage;
        private IWebElement buyerAccount;
        private IWebElement sellerAccount;
        private IWebElement contactName;
        private IWebElement companyName;
        private IWebElement address;
        private IWebElement city;
        private SelectElement country;
        private SelectElement state;
        private IWebElement zip;
        private IWebElement retailNumber;
        private IWebElement phoneNumber;
        private IWebElement website;
        private IWebElement contactEmail;
        private IWebElement confirmEmail;
        private IWebElement description;
        private IWebElement termConditionsCheckbox;
        private IWebElement termsConditionsPage;
        private IWebElement submit;
        private IWebElement backToLogin;
        private IWebElement confirmationMessage;
        private IWebElement goToLoginFromMsg;

        #endregion

        public CreateAccountPage(RemoteWebDriver driver) : base(driver)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_registrationFormID)));
            InitWebElements();
        }

        public void SetAccountType(AccountTypes accountType)
        {
            switch(accountType)
            {
                case AccountTypes.BUYER:
                    buyerAccount.Click();
                    break;
                case AccountTypes.SELLER:
                    sellerAccount.Click();
                    break;              
            }
        }

        public void SetContactName(string value)
        {
            contactName.SendKeys(value);
        }

        public void SetCompany(string value)
        {
            companyName.SendKeys(value);
        }

        public void SetAddress(string value)
        {
            address.SendKeys(value);
        }

        public void SetCity(string value)
        {
            city.SendKeys(value);
        }

        public void SetContryDropdown(string value)
        {
            country.SelectByText(value);           
        }

        public void SetStateDropdown(string value)
        {
            state.SelectByText(value);
        }

        public void SetZipCode(string value)
        {
            zip.SendKeys(value);
        }

        public void SetRetail(string value)
        {
            retailNumber.SendKeys(value);
        }

        public void SetPhone(string value)
        {
            phoneNumber.SendKeys(value);
        }

        public void SetWebsite(string value)
        {
            website.SendKeys(value);
        }

        public void SetEmail(string value)
        {
            contactEmail.SendKeys(value);
        }

        public void SetConfirmEmail(string value)
        {
            confirmEmail.SendKeys(value);
        }

        public void SetDescription(string value)
        {
            description.SendKeys(value);
        }

        public void TermsCheckbox()
        {
            termConditionsCheckbox.Click();
        }

        public void TermsConditionsPage()
        {
            termsConditionsPage.Click();
        }

        public void SubmitClick()
        {
            submit.Click();
            Thread.Sleep(2000);
            var wait = new WebDriverWait(WebDriverHelper.Driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.TextToBePresentInElement(WebDriver.FindElementById("messageform"), "Your request was sent for processing."));
        }

        public void BackToLogInClick()
        {
            backToLogin.Click();
        }

        private void GoToLoginClick()
        {
            goToLoginFromMsg.Click();
        }

        private void InitWebElements()
        {
            registrationPage = WebDriver.FindElementById(_registrationFormID);
            buyerAccount = WebDriver.FindElementById(_createBuyerID);
            sellerAccount = WebDriver.FindElementById(_createSellerID);
            contactName = WebDriver.FindElementById(_contactNameID);
            companyName = WebDriver.FindElementById(_companyNameID);
            address = WebDriver.FindElementById(_streetAddressID);
            city = WebDriver.FindElementById(_cityID);
            country = new SelectElement(WebDriver.FindElementById(_countryID));
            state = new SelectElement(WebDriver.FindElementById(_stateID));
            zip = WebDriver.FindElementById(_zipcodeID);
            retailNumber = WebDriver.FindElementById(_retailID);
            phoneNumber = WebDriver.FindElementById(_phoneNumberID);
            website = WebDriver.FindElementById(_companywebsiteID);
            contactEmail = WebDriver.FindElementById(_contactEmailID);
            confirmEmail = WebDriver.FindElementById(_confirmEmailID);
            description = WebDriver.FindElementById(_descriptionID);
            termConditionsCheckbox = WebDriver.FindElementById(_termConditionsCheckboxID);
            termsConditionsPage = WebDriver.FindElementById(_termsAndConditionsPageID);
            submit = WebDriver.FindElementById(_submitRequestID);
            backToLogin = WebDriver.FindElementById(_goLoginCreateAccID);          
        }
    }
}
