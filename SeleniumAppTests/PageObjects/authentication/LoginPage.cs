﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using SeleniumAppTests.PageObjects.authenticated;
using SeleniumAppTests.PageObjects.authenticated.home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects.authentication
{
    public class LoginPage : BasePage
    {
        #region Web Elements Identifiers

        private String _loginFormID = "loginform";
        private String _usernameInputID = "emailaddress";
        private String _passwordInputID = "password";
        private String _loginButtonID = "login";
        private String _forgotPassID = "forgotpassword";
        private String _requestNewAccID = "requestnewaccount";

        #endregion

        #region Web Elements 

        private IWebElement loginForm;
        private IWebElement username;
        private IWebElement password;
        private IWebElement loginButton;
        private IWebElement forgotPassword;
        private IWebElement requestAccount;

        #endregion

        public LoginPage(RemoteWebDriver driver) : base(driver)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_loginFormID)));
            initWebElements();
        }

        public void setUsername(string value)
        {           
            username.SendKeys(value);
        }

        public void setPassword(string value)
        {
            password.SendKeys(value);           
        }

        public HomePage loginClick()
        {
            loginButton.Click();
            return new HomePage(WebDriverHelper.Driver);
        }

        public ForgotPasswordPage forgotPassClick()
        {
            forgotPassword.Click();

            return new ForgotPasswordPage(WebDriverHelper.Driver);
        }

        public CreateAccountPage newAccountClick()
        {
            requestAccount.Click();

            return new CreateAccountPage(WebDriverHelper.Driver);
        }

        private void initWebElements()
        {
            loginForm = WebDriver.FindElementById(_loginFormID);
            username = WebDriver.FindElementById(_usernameInputID);
            password = WebDriver.FindElementById(_passwordInputID);
            loginButton = WebDriver.FindElementById(_loginButtonID);
            forgotPassword = WebDriver.FindElementById(_forgotPassID);
            requestAccount = WebDriver.FindElementById(_requestNewAccID);
        }

    }

} 
