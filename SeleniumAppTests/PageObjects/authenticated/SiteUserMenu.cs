﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace SeleniumAppTests.PageObjects.authenticated
{
    public class SiteUserMenu
    {
        #region  Web Elements Identifiers

        private String _userMenuID = "usermenu";
        private String _userMenuWrapperID = "usermenuwrapper";

        #endregion

        #region  Web Elements

        private IWebElement userMenu;

        #endregion

        public SiteUserMenu (RemoteWebDriver driver)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_userMenuWrapperID)));
            initWebElements();
        }

        public void Dashboard()
        {
            Open();
            WebDriverHelper.Driver.FindElementByLinkText("Dashboard").Click();
        }

        public void AccountSettings()
        {

            Open();
            WebDriverHelper.Driver.FindElementByLinkText("Account Settings").Click();
        }

        public void SignOut()
        {
            Open();
            WebDriverHelper.Driver.FindElementByLinkText("Sign out").Click();
        }

        private void initWebElements()
        {
            userMenu = WebDriverHelper.Driver.FindElementById(_userMenuID);
        }

        private void Open()
        {
            userMenu.Click();
            Thread.Sleep(500);
        }
    }
}
