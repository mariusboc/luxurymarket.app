﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SeleniumAppTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumAppTests.PageObjects.authenticated
{
    public class SiteMenu
    {
        #region Web Element Identifiers

        private String _userSiteMenuID = "sitemenu";

        #endregion

        #region

        private IWebElement siteMenu;

        #endregion

        public SiteMenu(RemoteWebDriver driver)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(AppConfig.WaitForPageLoad));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id(_userSiteMenuID)));
            InitWebElements();
        }

        public void CategoryMenu()
        {
            siteMenu.FindElement(By.LinkText("FEATURED"));                   
            siteMenu.FindElement(By.LinkText("MEN'S SHOP"));
            siteMenu.FindElement(By.LinkText("BRANDS"));
            siteMenu.FindElement(By.LinkText("SEARCH"));
        }

        public void Women()
        {
            siteMenu.FindElement(By.LinkText("WOMEN'S APPAREL")).Click();
        }

        public void CategoryMenuAccesories()
        {
            siteMenu.FindElement(By.LinkText("HANDBAGS / ACCESORIES")).Click();
        }

        public void CategoryFeatured(string value)
        {
            siteMenu.FindElements(By.LinkText("BRANDS"));
        }




        private void InitWebElements()
        {
            siteMenu = WebDriverHelper.Driver.FindElementById(_userSiteMenuID);    
        }
    }
}
