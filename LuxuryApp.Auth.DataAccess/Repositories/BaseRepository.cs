﻿//using System;
//using System.Configuration;
//using System.Data;
//using System.Data.SqlClient;
//using System.Threading.Tasks;

//namespace LuxuryApp.Auth.DataAccess.Repositories
//{
//    public class BaseRepository
//    {
//        SqlConnection _connection;
//        readonly string _connectionString = ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["ConnectionStringName"]].ConnectionString;

//        public SqlConnection Connection
//        {
//            get
//            {
//                if (string.IsNullOrWhiteSpace(this._connectionString))
//                {
//                    throw new Exception("connection string missing");
//                }

//                this._connection = new SqlConnection(_connectionString);
//                return _connection;
//            }
//        }

//        protected async Task<T> WithConnection<T>(Func<IDbConnection, Task<T>> getData)
//        {
//            try
//            {
//                using (var connection = this.Connection)
//                {
//                    await connection.OpenAsync();
//                    return await getData(connection);
//                }
//            }
//            catch (TimeoutException ex)
//            {
//                throw new Exception($"{GetType().FullName}.WithConnection() experienced a SQL timeout", ex);
//            }
//            catch (SqlException ex)
//            {
//                throw new Exception($"{GetType().FullName}.WithConnection() experienced a SQL exception (not a timeout)", ex);
//            }
//        }

//        protected T WithSyncConnection<T>(Func<IDbConnection, T> getData)
//        {
//            try
//            {
//                using (var connection = this.Connection)
//                {
//                    connection.Open();
//                    return getData(connection);
//                }
//            }
//            catch (TimeoutException ex)
//            {
//                throw new Exception($"{GetType().FullName}.WithConnection() experienced a SQL timeout", ex);
//            }
//            catch (SqlException ex)
//            {
//                throw new Exception($"{GetType().FullName}.WithConnection() experienced a SQL exception (not a timeout)", ex);
//            }
//        }
//    }
//}
