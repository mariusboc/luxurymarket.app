﻿using LuxuryApp.Auth.Core.Interfaces;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using LuxuryApp.Auth.Core.Entities;

namespace LuxuryApp.Auth.DataAccess.Repositories
{
    public class RefreshTokenRepository : DapperRepository<RefreshToken>, IRefreshTokenRepository
    {
        public async Task<List<RefreshToken>> GetAllAsync()
        {
            string query = "SELECT * FROM app.RefreshTokens";
            var result = await QueryAsync<RefreshToken>(query, null);
            return result.ToList();
        }
        public async Task<IEnumerable<RefreshToken>> FindBySubjectAsync(string subject)
        {
            string query = "SELECT * FROM app.RefreshTokens WHERE Subject=@Subject";

            var result = await QueryAsync<RefreshToken>(query, new { @Subject = subject });

            return result;
        }

        public async Task<bool> DeleteAsync(string refreshTokenId)
        {
            var item = await FindAsync(x => x.Id == refreshTokenId);
            if (item.Any())
            {
                return await RemoveAsync(item.FirstOrDefault());
            }
            return false;

        }

        public async Task<bool> RenewAsync(RefreshToken token)
        {
            var existingToken = await FindBySubjectAsync(token.Subject);

            if (existingToken != null)
            {
                await RemoveAsync(existingToken.FirstOrDefault());
            }

            await InsertAsync(token);
            return true;
        }

    }
}
