﻿using LuxuryApp.Auth.Core.Entities;
using System;
using System.Data;
using LuxuryApp.Auth.Core.Interfaces;

namespace LuxuryApp.Auth.DataAccess
{

    public class DapperIdentityDbContext<TUser, TRole> : IdentityDbContext<TUser, TRole, int, string>
        where TUser : User<int>
        where TRole : Role<string>
    {
        public DapperIdentityDbContext(IDbConnection connection) : base(connection, null)
        {

        }
        public DapperIdentityDbContext(IDbConnection connection, IDbTransaction transaction) : base(connection, transaction)
        {
        }
    }

    public class IdentityDbContext<TUser, TRole, TKey, TRoleKey> : IDisposable
        where TUser : User<TKey>
        where TRole : Role<TRoleKey>
    {
        private IDbConnection _conn;
        private IDbTransaction _transaction;


        private IRepository<TUser, TKey> _userRepository;
        private IRepository<UserLogin<TKey>> _userLoginRepository;
        private IRepository<UserClaim<TKey>> _userClaimRepository;
        private IRepository<TRole, TRoleKey> _roleRepository;
        private IRepository<UserRole<TKey, TRoleKey>> _userRoleRepository;
        //private IRepository<RefreshToken> _refreshTokenRepository;

        public IRepository<TUser, TKey> UserRepository
        {
            get
            {
                return _userRepository ?? (_userRepository = new DapperRepository<TUser, TKey>(DbConnection, DbTransaction));
            }
        }
        public IRepository<TRole, TRoleKey> RoleRepository
        {
            get
            {
                return _roleRepository ?? (_roleRepository = new DapperRepository<TRole, TRoleKey>(DbConnection, DbTransaction));
            }
        }
        public IRepository<UserRole<TKey, TRoleKey>> UserRoleRepository
        {
            get
            {
                return _userRoleRepository ?? (_userRoleRepository = new DapperRepository<UserRole<TKey, TRoleKey>>(DbConnection, DbTransaction));
            }
        }
        public IRepository<UserLogin<TKey>> UserLoginRepository
        {
            get
            {
                return _userLoginRepository ?? (_userLoginRepository = new DapperRepository<UserLogin<TKey>>(DbConnection, DbTransaction));
            }
        }
        public IRepository<UserClaim<TKey>> UserClaimRepository
        {
            get
            {
                return _userClaimRepository ?? (_userClaimRepository = new DapperRepository<UserClaim<TKey>>(DbConnection, DbTransaction));
            }
        }
        //public IRepository<RefreshToken> RefreshTokenRepository
        //{
        //    get
        //    {
        //        return _refreshTokenRepository ?? (_refreshTokenRepository = new DapperRepository<RefreshToken>(DbConnection, DbTransaction));
        //    }
        //}

        public IDbConnection DbConnection
        {
            get
            {
                return _conn;
            }
        }
        public IDbTransaction DbTransaction
        {
            get
            {
                return _transaction;
            }
        }

        public IdentityDbContext(IDbConnection connection) : this(connection, null)
        {

        }
        public IdentityDbContext(IDbConnection connection, IDbTransaction transaction)
        {
            if (connection == null)
                throw new ArgumentNullException("connection");
            _conn = connection;
            _transaction = transaction;
        }

        public void Dispose()
        {
            if (_conn.State != ConnectionState.Closed)
            {
                _conn.Close();
            }
        }
    }
}
