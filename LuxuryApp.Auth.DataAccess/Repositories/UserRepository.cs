﻿using LuxuryApp.Auth.Core.Interfaces;
using System.Linq;
using Dapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LuxuryApp.Auth.Core.Entities;
using Microsoft.AspNet.Identity;
using System.Data;

namespace LuxuryApp.Auth.DataAccess.Repositories
{
    public class UserRepository : DapperRepository<User>, IUserRepository
    {
        // <summary>
        /// SELECT operation for finding a user by the username.
        /// </summary>
        /// <param name="userName">The username of the user object.</param>
        /// <returns>Returns the User object for the supplied username or null.</returns>
        public async Task<User> FindByUserNameAsync(string userName)
        {
            string query = "SELECT * FROM app.Users WHERE LOWER(UserName)=LOWER(@UserName)";
            var user = await QueryAsync<User>(query, new { @UserName = userName });
            return user.SingleOrDefault();
        }

        public async Task<User> FindByEmailAsync(string email)
        {
            string query = "SELECT * FROM app.Users WHERE LOWER(Email)=LOWER(@Email)";
            var user = await QueryAsync<User>(query, new { @Email = email });
            return user.SingleOrDefault();
        }

        public async Task<User> FindUserAsync(string username, string password)
        {
            string query = "SELECT * FROM app.Users WHERE LOWER(Email)=LOWER(@Email) or LOWER(UserName)=LOWER(UserName) and PasswordHash=@Password";
            var user = await QueryAsync<User>(query, new { @Email = username, @UserName = username, @Password = password });
            return user.SingleOrDefault();
        }

        //public async Task<User> ChangePasswordForUserAsync(int Id, string password, string securityStamp)
        //{
        //    string query = "UPDATE app.Users SET PasswordHash=@Password, SecurityStamp=@SecurityStamp WHERE Id=@Id SELECT * FROM app.Users WHERE Id=@Id";
        //    var user = await QueryAsync<User>(query, new { @Id = Id, @Password = password, @SecurityStamp = securityStamp });
        //    return user.SingleOrDefault();
        //}

        ///// <summary>
        ///// Method for setting the password hash for the user account.  This hash is used to encode the app.Users password.
        ///// </summary>
        ///// <param name="user">The user to has the password for.</param>
        ///// <param name="passwordHash">The password has to use.</param>
        ///// <returns></returns>
        //public Task SetPasswordHashAsync(User user, string passwordHash)
        //{
        //    if (user == null)
        //    {
        //        throw new ArgumentNullException(nameof(user));
        //    }
        //    user.PasswordHash = passwordHash;
        //    return Task.FromResult(0);
        //}

        ///// <summary>
        ///// Method for getting teh password hash for the user account.
        ///// </summary>
        ///// <param name="user">The user to get the password hash for.</param>
        ///// <returns>The password hash.</returns>
        //public Task<string> GetPasswordHashAsync(User user)
        //{
        //    if (user == null)
        //    {
        //        throw new ArgumentNullException(nameof(user));
        //    }
        //    return Task.FromResult(user.PasswordHash);
        //}

        ///// <summary>
        ///// Method for checking if an account has a password hash.
        ///// </summary>
        ///// <param name="user">The user to check for an existing password hash.</param>
        ///// <returns>True of false depending on whether the password hash exists or not.</returns>
        //public Task<bool> HasPasswordAsync(User user)
        //{
        //    return Task.FromResult(!string.IsNullOrEmpty(user.PasswordHash));
        //}

        ///// <summary>
        ///// Method for setting the security stamp for the user account.
        ///// </summary>
        ///// <param name="user">The user to set the security stamp for.</param>
        ///// <param name="stamp">The stamp to set.</param>
        ///// <returns></returns>
        //public Task SetSecurityStampAsync(User user, string stamp)
        //{
        //    if (user == null)
        //    {
        //        throw new ArgumentNullException(nameof(user));
        //    }
        //    user.SecurityStamp = stamp;
        //    return Task.FromResult(0);
        //}

        ///// <summary>
        ///// Method for getting the security stamp for the user account.
        ///// </summary>
        ///// <param name="user">The user to get the security stamp for.</param>
        ///// <returns>The security stamp.</returns>
        //public Task<string> GetSecurityStampAsync(User user)
        //{
        //    if (user == null)
        //    {
        //        throw new ArgumentNullException(nameof(user));
        //    }
        //    return Task.FromResult(user.SecurityStamp);
        //}

    }
}