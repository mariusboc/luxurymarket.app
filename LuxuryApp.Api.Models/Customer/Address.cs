﻿using System.ComponentModel.DataAnnotations;

namespace LuxuryApp.Api.Models.Customer
{
    public class Address : EntityBase
    {
        [Required]
        public string City { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        public string StreetAddress { get; set; }

        [Required]
        public string ZipCode { get; set; }

        [Required]
        public string Country { get; set; }
    }
}
