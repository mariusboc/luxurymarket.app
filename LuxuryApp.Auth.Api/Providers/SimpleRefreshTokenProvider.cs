﻿using System;
using System.Threading.Tasks;
using System.Web.Http.Results;
using LuxuryApp.Auth.Api.Helpers;
using LuxuryApp.Auth.Core.Entities;
using LuxuryApp.Auth.DataAccess.Repositories;
using Microsoft.Owin.Security.Infrastructure;
using LuxuryApp.Auth.Api.Models;
using System.Linq;

namespace LuxuryApp.Auth.Api.Providers
{
    public class SimpleRefreshTokenProvider : IAuthenticationTokenProvider
    {
        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            var refreshTokenId = Guid.NewGuid().ToString("n");

            using (var repo = new RefreshTokenRepository())
            {
                var refreshTokenLifeTime = context.OwinContext.Get<string>("as:clientRefreshTokenLifeTime");

                var token = new RefreshToken
                {
                    Id = refreshTokenId,
                    Subject = context.Ticket.Identity.Name,
                    IssuedUtc = DateTime.Now,
                    ExpiresUtc = DateTime.Now.AddDays(1)//.AddMinutes(Convert.ToDouble(refreshTokenLifeTime))
                };

                context.Ticket.Properties.IssuedUtc = token.IssuedUtc;
                context.Ticket.Properties.ExpiresUtc = token.ExpiresUtc;

                token.ProtectedTicket = context.SerializeTicket();

                if (RenewToken(token, repo))
                {
                    context.SetToken(refreshTokenId);
                }
            }
        }

        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

            var hashedTokenId = EncryptHelper.GetHash(context.Token);

            using (var repo = new RefreshTokenRepository())
            {
                var refreshTokens =await repo.FindAsync(x => x.Id == hashedTokenId);
                var item = refreshTokens.FirstOrDefault();
                if (item != null)
                {
                    context.DeserializeTicket(item.ProtectedTicket);
                    await repo.DeleteAsync(hashedTokenId);
                }
            }
        }

        public void Create(AuthenticationTokenCreateContext context)
        {
            throw new NotImplementedException();
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            throw new NotImplementedException();
        }

        private bool RenewToken(RefreshToken token, RefreshTokenRepository repo)
        {
            try
            {
                var existingToken = repo.FindBySubjectAsync(token.Subject);

                if (existingToken != null)
                {
                    var result = repo.DeleteAsync(token.Subject);
                    Console.WriteLine("Delete token =" + result);
                }

                repo.Insert(token);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}