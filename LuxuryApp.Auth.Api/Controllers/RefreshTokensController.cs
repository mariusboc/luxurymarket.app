﻿using System.Threading.Tasks;
using System.Web.Http;
using LuxuryApp.Auth.DataAccess.Repositories;

namespace LuxuryApp.Auth.Api.Controllers
{
    [RoutePrefix("api/RefreshTokens")]
    public class RefreshTokensController : ApiController
    {
        private readonly RefreshTokenRepository _repo;

        public RefreshTokensController()
        {
            _repo = new RefreshTokenRepository();
        }

        [Authorize]
        [Route("")]
        public IHttpActionResult Get()
        {
            return Ok(_repo.GetAllAsync());
        }

        [Authorize]
        [Route("")]
        public async Task<IHttpActionResult> Delete(string tokenId)
        {
            await _repo.DeleteAsync(tokenId);
            return Ok();
        }
    }
}
