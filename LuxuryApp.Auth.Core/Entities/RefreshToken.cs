﻿using LuxuryApp.Auth.Core.Attributes;
using System;

namespace LuxuryApp.Auth.Core.Entities
{
    [Table("app.RefreshTokens")]
    public class RefreshToken
    {
        public string Id { get; set; }
        public string Subject { get; set; }
        public DateTime IssuedUtc { get; set; }
        public DateTime ExpiresUtc { get; set; }
        public string ProtectedTicket { get; set; }
    }
}