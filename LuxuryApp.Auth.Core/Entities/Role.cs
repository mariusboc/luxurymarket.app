﻿using Microsoft.AspNet.Identity;
using System;

namespace LuxuryApp.Auth.Core.Entities
{
    public class Role:Role<string>
    {
        public Role()
        {
            Id = Guid.NewGuid().ToString();
        }
        public Role(string name)
            : base(Guid.NewGuid().ToString(),name)
        {
        }
        public Role(string id, string name)
            : base(id,name)
        {
        }
    }
    public class Role<TKey> : IRole<TKey>
    {
        /// <summary>
        /// Default constructor for Role 
        /// </summary>
        public Role()
        {
        }
        /// <summary>
        /// Constructor that takes names as argument 
        /// </summary>
        /// <param name="name"></param>
        public Role(string name)
            : this()
        {
            Name = name;
        }

        public Role(TKey id,string name):this(name)
        {
            Id = id;
        }

        /// <summary>
        /// Role ID
        /// </summary>
        public TKey Id { get; set; }

        /// <summary>
        /// Role name
        /// </summary>
        public string Name { get; set; }
    }
}
