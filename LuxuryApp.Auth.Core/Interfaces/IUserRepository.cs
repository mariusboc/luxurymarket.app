﻿using LuxuryApp.Auth.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Auth.Core.Interfaces
{
    public interface IUserRepository
    {
        Task<User> FindByUserNameAsync(string userName);

        Task<User> FindByEmailAsync(string email);

        Task<User> FindUserAsync(string username, string password);

        //Task SetPasswordHashAsync(User user, string passwordHash);

        //Task<string> GetPasswordHashAsync(User user);
    }
}
