﻿namespace LuxuryApp.Core.Infrastructure.Enums
{
    /// <summary>
    /// Error Code
    /// </summary>
    public enum ErrorCode
    {
        /// <summary>
        /// Item not found
        /// </summary>
        NotFound = 1,

        /// <summary>
        /// Item not found
        /// </summary>
        EntityNotFound = 2,
        /// <summary>
        /// Bad request
        /// </summary>
        BadRequest = 3,

        /// <summary>
        /// Illegal operation
        /// </summary>
        IllegalOperation = 4,

        /// <summary>
        /// Invalid Request
        /// </summary>
        InvalidRequest = 5,

        FailedToCreate = 6,

        FailedToUpdate = 7,

        FailedToDelete = 8
    }
}