﻿using LuxuryApp.Core.Infrastructure.Api.Exceptions;
using LuxuryApp.Core.Infrastructure.Enums;
using LuxuryApp.Core.Infrastructure.Extensions;
using System.Net;

namespace LuxuryApp.Core.Infrastructure.Exceptions
{
    /// <summary>
    /// Luxury Api exception
    /// </summary>
    public class LuxuryApiException : ApiException
    {       /// <summary>
            /// Creates a new PaymentApiException instance
            /// </summary>
        public LuxuryApiException(ErrorCode errCode, string message, HttpStatusCode httpStatusCode, string moreInfo) : base(message)
        {
            ErrorCode = errCode.ToErrorCodeString();
            HttpStatusCode = httpStatusCode;
            MoreInfo = moreInfo;
        }
    }
}