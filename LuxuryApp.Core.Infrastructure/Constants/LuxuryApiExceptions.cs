﻿using LuxuryApp.Core.Infrastructure.Enums;
using System.Collections.Generic;
using System.Net;

namespace LuxuryApp.Core.Infrastructure.Constants
{
    /// <summary>
    /// Contains Api Errors definitions
    /// </summary>
    public static class LuxuryApiExceptions
    {
        /// <summary>
        /// Predefined exception types
        /// </summary>
        public static Dictionary<ErrorCode, LuxuryApiExceptionType> Types = new Dictionary<ErrorCode, LuxuryApiExceptionType>
        {
            { ErrorCode.NotFound,new LuxuryApiExceptionType { ErrorCode= ErrorCode.NotFound,
                Description ="Unable to locate the resource",
                HttpStatusCode =HttpStatusCode.NotFound }
            },
            { ErrorCode.EntityNotFound,new LuxuryApiExceptionType { ErrorCode= ErrorCode.EntityNotFound,
                Description ="Unable to locate the item in the database",
                HttpStatusCode =HttpStatusCode.NotFound }
            },
            { ErrorCode.InvalidRequest,new LuxuryApiExceptionType { ErrorCode= ErrorCode.InvalidRequest,
                Description ="Invalid api request, request parameters does not match the business rules",
                HttpStatusCode =HttpStatusCode.PreconditionFailed }
            },
               { ErrorCode.FailedToCreate,new LuxuryApiExceptionType { ErrorCode= ErrorCode.FailedToCreate,
                Description ="Failed to create entity in database",
                HttpStatusCode =HttpStatusCode.BadRequest }
            },
            { ErrorCode.FailedToUpdate,new LuxuryApiExceptionType { ErrorCode= ErrorCode.FailedToUpdate,
                Description ="Failed to update entity in database",
                HttpStatusCode =HttpStatusCode.BadRequest }
            },
            { ErrorCode.FailedToDelete,new LuxuryApiExceptionType { ErrorCode= ErrorCode.FailedToDelete,
                Description ="Failed to delete entity in database",
                HttpStatusCode =HttpStatusCode.BadRequest }
            }
        };

        /// <summary>
        /// Not found in core exception type
        /// </summary>
        public static LuxuryApiExceptionType NotFoundInDatabase
        {
            get { return Types[ErrorCode.EntityNotFound]; }
        }

        public static LuxuryApiExceptionType NotFound
        {
            get { return Types[ErrorCode.NotFound]; }
        }

        /// <summary>
        /// Bad request exception type
        /// </summary>
        public static LuxuryApiExceptionType InvalidRequest
        {
            get { return Types[ErrorCode.InvalidRequest]; }
        }

        /// <summary>
        /// Failed to create entity exception type
        /// </summary>
        public static LuxuryApiExceptionType FailedToCreate
        {
            get { return Types[ErrorCode.FailedToCreate]; }
        }

        /// <summary>
        /// Failed to update entity exception type
        /// </summary>
        public static LuxuryApiExceptionType FailedToUpdate
        {
            get { return Types[ErrorCode.FailedToUpdate]; }
        }

        /// <summary>
        /// Failed to delete entity exception type
        /// </summary>
        public static LuxuryApiExceptionType FailedToDelete
        {
            get { return Types[ErrorCode.FailedToDelete]; }
        }

    }

    /// <summary>
    /// Payment api exception type definition
    /// </summary>
    public class LuxuryApiExceptionType
    {
        /// <summary>
        /// Error code
        /// </summary>
        public ErrorCode ErrorCode { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Http status code
        /// </summary>
        public HttpStatusCode HttpStatusCode { get; set; }
    }
}