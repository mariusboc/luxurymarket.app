﻿using LuxuryApp.Core.Infrastructure.Enums;

namespace LuxuryApp.Core.Infrastructure.Extensions
{
    public static class EnumExtensions
    {
        public static string ToErrorCodeString(this ErrorCode err)
        {
            return ((int)err).ToString();
        }
    }
}