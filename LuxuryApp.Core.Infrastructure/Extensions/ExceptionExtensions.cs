﻿
using LuxuryApp.Core.Infrastructure.Constants;
using LuxuryApp.Core.Infrastructure.Exceptions;

namespace LuxuryApp.Core.Infrastructure.Extensions
{
    public static class ExceptionExtensions
    {
        public static LuxuryApiException ToLuxuryApiException(this LuxuryApiExceptionType exceptionType, string message)
        {
            return new LuxuryApiException(exceptionType.ErrorCode, message, exceptionType.HttpStatusCode, string.Empty);
        }

        public static LuxuryApiException ToLuxuryApiException(this LuxuryApiExceptionType exceptionType, string format, params object[] args)
        {
            return exceptionType.ToLuxuryApiException(string.Format(format, args));
        }
    }
}