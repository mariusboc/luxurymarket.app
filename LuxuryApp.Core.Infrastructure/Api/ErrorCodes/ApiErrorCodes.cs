﻿namespace LuxuryApp.Core.Infrastructure.Api.ErrorCodes
{
    /// <summary>
    /// Error Code
    /// </summary>
    public enum ApiErrorCodes
    {
        /// <summary>
        /// Item not found
        /// </summary>
        NotFound = 1,

        /// <summary>
        /// Bad request
        /// </summary>
        BadRequest = 2
    }
}
