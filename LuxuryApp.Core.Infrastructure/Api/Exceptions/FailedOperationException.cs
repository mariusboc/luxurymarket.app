﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Core.Infrastructure.Api.Exceptions
{
    [Serializable]
    public class FailedOperationException : Exception
    {
        public FailedOperationException()
        {
        }

        public FailedOperationException(string message)
            : base(message)
        {
        }

        public FailedOperationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected FailedOperationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
