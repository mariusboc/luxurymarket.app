﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Core.Infrastructure.Api.Exceptions
{
    [Serializable]
    public class IllegalOperationException : Exception
    {
        public IllegalOperationException()
        {
        }

        public IllegalOperationException(string message)
            : base(message)
        {
        }

        public IllegalOperationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected IllegalOperationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
