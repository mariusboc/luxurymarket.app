﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Core.Infrastructure.Api.Exceptions
{
    [Serializable]
    public class OperationValidationException : Exception
    {
        public OperationValidationException()
        {
        }

        public OperationValidationException(string message)
            : base(message)
        {
        }

        public OperationValidationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected OperationValidationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
