﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.Core.Infrastructure.Api.Exceptions
{
    /// <summary>
    /// Thrown when the entities involved in an operation do not follow business validations.
    /// </summary>
    [Serializable]
    public class CustomValidationException : Exception
    {
        public CustomValidationException()
        {
        }

        public CustomValidationException(string message)
            : base(message)
        {
        }

        public CustomValidationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected CustomValidationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
