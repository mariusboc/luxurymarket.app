﻿using System;
using System.Net;
using System.Runtime.Serialization;

namespace LuxuryApp.Core.Infrastructure.Api.Exceptions
{
    [Serializable]
    public class ApiException : Exception
    {
        public string ErrorCode { get; set; }
        public HttpStatusCode HttpStatusCode { get; set; }
        public string MoreInfo { get; set; }

        public ApiException(string message) : base(message)
        {
            HttpStatusCode = HttpStatusCode.BadRequest;
        }

        public ApiException(string message, Exception innerException) : base(message, innerException)
        {
            HttpStatusCode = HttpStatusCode.BadRequest;
        }

        protected ApiException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            HttpStatusCode = HttpStatusCode.BadRequest;
        }
    }
}
