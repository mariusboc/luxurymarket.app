﻿using System;

namespace LuxuryApp.Core.Infrastructure.Api.Exceptions
{
    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException(int id, Type type)
        {
            this.Id = id;
            this.Type = type;
        }

        public int Id { get; set; }

        public Type Type { get; set; }

        public override string ToString()
        {
            return Type.Name + " with id " + Id + " was not found.";
        }
    }
}
