﻿using System.Configuration;
using System.IdentityModel;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using LuxuryApp.Core.Infrastructure.Api.Exceptions;

namespace LuxuryApp.Core.Infrastructure.Api.Models
{
    public class ErrorMessageResult : IHttpActionResult
    {
        public HttpRequestMessage Request { get; set; }
        public string Content { get; set; }
        public ExceptionHandlerContext Context { get; set; }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var statusCode = HttpStatusCode.InternalServerError;
            var errorCode = string.Empty;

            var apiException = Context.Exception as ApiException;
            if (apiException != null)
            {
                if (apiException.HttpStatusCode != 0)
                    statusCode = apiException.HttpStatusCode;

                errorCode = apiException.ErrorCode;
            }
            else if (Context.Exception is BadRequestException)
            {
                statusCode = HttpStatusCode.BadRequest;
            }
            else if (Context.Exception is ResourceNotFoundException)
            {
                statusCode = HttpStatusCode.NotFound;
            }

            var stackTrace = "";
            var environment = ConfigurationManager.AppSettings["Smart.Environment"];
            if (!string.IsNullOrEmpty(environment) && !environment.Equals("Live", System.StringComparison.InvariantCultureIgnoreCase))
            {
                stackTrace = Context.Exception.StackTrace;
            }

            var response = new HttpResponseMessage
            {
                Content = new ObjectContent<ErrorResponse>(
                    new ErrorResponse
                    {
                        ErrorCode = string.IsNullOrEmpty(errorCode) ? ((int)statusCode).ToString() : errorCode,
                        Message = string.IsNullOrEmpty(Content) ? "Internal server error" : Content,
                        MoreInfo = stackTrace
                    },
                    new JsonMediaTypeFormatter(), "application/json"),
                RequestMessage = Request,
                StatusCode = statusCode
            };

            return Task.FromResult(response);
        }

    }
}
