﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Mail;
namespace LuxuryApp.NotificationService
{
    public class EmailChannelDistribution :ChannelDistribution
    {
        public string From { get; set; }

        public string FromName { get; set; }

        public string To { get; set; }

        public string Cc { get; set; }

        public string Subject { get; set; }

        public string EmailTemplateName { get; set; }

        public string EmailTemplateContent { get; set; }

        public object DataBag { get; set; }

        /// <summary>
        /// If this is set to true then the default master will not be used for email templates
        /// </summary>
        public bool HasOwnMasterTemplate { get; set; }

        [JsonIgnore]
        public IEnumerable<Attachment> Attachments { get; set; }
    }
}
