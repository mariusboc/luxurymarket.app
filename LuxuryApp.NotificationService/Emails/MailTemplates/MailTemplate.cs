﻿//using System;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.Globalization;
//using System.IO;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace LuxuryApp.NotificationService.Emails.MailTemplates
//{
//    public class MailTemplate
//    {
//        public static string GetParsedContent(string fileName, object tokens, bool hasOwnMasterTemplate)
//        {
//            if (string.IsNullOrWhiteSpace(fileName)) throw new ArgumentException("Email template name must be provided", nameof(fileName));

//            string content;
//            var filePath = BuildPathFor(fileName);
//            var tokenList = MessageTemplate.BuildTokenValuePairs(tokens);

//            try
//            {
//                content = MessageTemplate.ReplaceTokens(tokenList, File.ReadAllText(filePath));
//            }
//            catch (Exception ex)
//            {
//                Trace.TraceError(
//                    string.Format(
//                        CultureInfo.InvariantCulture,
//                        "Could not read and parse file {0}, reason: {1}, stack trace: {2}",
//                        filePath,
//                        ex.Message,
//                        ex.StackTrace));
//                throw;
//            }

//            if (string.IsNullOrWhiteSpace(content))
//            {
//                throw new MailTemplateReaderException(
//                    string.Format(
//                        CultureInfo.InvariantCulture,
//                        "Email content at {0} cannot be empty.",
//                        fileName));
//            }

//            string contentResult = hasOwnMasterTemplate ? content : GetMasterContent(content);

//            return contentResult;
//        }

//        public static string GetParsedContentFromTemplateContent(string templateContent, object tokens)
//        {
//            string content;
//            var tokenList = MessageTemplate.BuildTokenValuePairs(tokens);

//            try
//            {
//                content = MessageTemplate.GetParsedContentFromTemplateContent(templateContent, tokens);
//            }
//            catch (Exception ex)
//            {
//                Trace.TraceError(
//                    string.Format(
//                        CultureInfo.InvariantCulture,
//                        "Could not parse template content: {0} for {1}, reason: {2}, stack trace: {3}",
//                        tokenList.FirstOrDefault(t => t.Key == "MessageType").Value,
//                        tokenList.FirstOrDefault(t => t.Key == "Center").Value,
//                        ex.Message,
//                        ex.StackTrace));
//                throw;
//            }

//            if (string.IsNullOrWhiteSpace(content))
//            {
//                throw new MailTemplateReaderException(
//                    string.Format(
//                        CultureInfo.InvariantCulture,
//                        "Email content {0} for {1} cannot be empty.",
//                        tokenList.FirstOrDefault(t => t.Key == "MessageType").Value,
//                        tokenList.FirstOrDefault(t => t.Key == "Center").Value));
//            }

//            var masterContent = GetMasterContent(content);

//            return masterContent;
//        }

//        private static string BuildPathFor(string file)
//        {
//            return
//                Path.Combine(
//                    new[]
//                        {
//                            Directory.GetCurrentDirectory(),
//                            "Channels\\Emails\\MailTemplates",
//                            string.Format(CultureInfo.InvariantCulture, "{0}.html", file)
//                        });
//        }

//        private static string GetMasterContent(string bodyContent)
//        {
//            var masterFilePath = BuildPathFor("Master");

//            var masterContent = File.ReadAllText(masterFilePath);

//            masterContent = masterContent.Replace("[@BODYCONTENT]", bodyContent);

//            return masterContent;
//        }
//    }
//}
