﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace LuxuryApp.NotificationService.Emails
//{
//    public class EmailChannelHandler<T> : IChannelHandler<T> where T : ChannelDistribution
//    {
//        public bool CanHandle(T context)
//        {
//            return context is EmailChannelDistribution;
//        }

//        public virtual void Handle(T context)
//        {
//            var emailDistribution = context as EmailChannelDistribution;
//            if (emailDistribution == null)
//                throw new ArgumentException("Invalid contex. Expected EmailChannelDistribution");

//            var body = string.Empty;
//            if (!string.IsNullOrEmpty(emailDistribution.EmailTemplateContent))//this is for dynamic templates 
//            {
//                body = MailTemplate.GetParsedContentFromTemplateContent(emailDistribution.EmailTemplateContent, emailDistribution.DataBag);
//            }
//            if (!string.IsNullOrEmpty(emailDistribution.EmailTemplateName))
//            {
//                body = MailTemplate.GetParsedContent(emailDistribution.EmailTemplateName, emailDistribution.DataBag, emailDistribution.HasOwnMasterTemplate);
//            }

//            bool isNotificationEnabled;
//            bool.TryParse(CloudConfigurationManager.GetSetting(NotificationCloudConfigurationKeys.IsNotificationEnabled), out isNotificationEnabled);

//            Trace.TraceInformation($@"[EmailChannelHandler] Sending email. Message: {emailDistribution.MessageGuid}, 
//template name: {emailDistribution.EmailTemplateName}, centerIsActive: {emailDistribution.IsActive}, azureEnabled {isNotificationEnabled}, 
//to: {emailDistribution.To}, cc:{emailDistribution.Cc}");

//            if (string.IsNullOrEmpty(emailDistribution.To))
//            {
//                throw new ArgumentException("Invalid contex. Expected an EmailChannelDistribution To value");
//            }

//            SendMail(emailDistribution.To, emailDistribution.Cc, emailDistribution.From, emailDistribution.FromName, emailDistribution.Subject, body, emailDistribution.IsActive, isNotificationEnabled, emailDistribution.Attachments, emailDistribution.HasOwnMasterTemplate);
//        }

//        public static void SendMail(string to, string cc, string from, string fromName, string subject, string body, bool sendToRecipient = false, bool isNotificationEnabled = false, IEnumerable<Attachment> attachements = null, bool hasOwnMasterTemplate = false)
//        {
//            try
//            {
//                var userName = CloudConfigurationManager.GetSetting("SendGrid-UserName");
//                var password = CloudConfigurationManager.GetSetting("SendGrid-Password");
//                var server = CloudConfigurationManager.GetSetting("SendGrid-Server");

//                var mailMsg = new MailMessage();

//                // To
//                var recipient = GetEmailReceipient(to, sendToRecipient && isNotificationEnabled);
//                foreach (var email in recipient.Split(','))
//                {
//                    mailMsg.To.Add(new MailAddress(email.Trim()));
//                }

//                // Cc
//                if (!string.IsNullOrWhiteSpace(cc) && isNotificationEnabled)
//                {
//                    foreach (var ccEmail in cc.Split(','))
//                    {
//                        mailMsg.CC.Add(new MailAddress(ccEmail.Trim()));
//                    }
//                }

//                // From
//                mailMsg.From = new MailAddress(@from, fromName);

//                // Subject and multipart/alternative Body
//                mailMsg.Subject = subject;

//                // Add attachements
//                if (attachements != null)
//                {
//                    foreach (var attachement in attachements)
//                        mailMsg.Attachments.Add(attachement);
//                }

//                // Build the actual email
//                var htmlAlternativeView = AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html);
//                if (!hasOwnMasterTemplate)
//                {
//                    var linkedResources = GetEmailLinkedResources();

//                    foreach (var linkedResource in linkedResources)
//                        htmlAlternativeView.LinkedResources.Add(linkedResource);
//                }

//                mailMsg.AlternateViews.Add(htmlAlternativeView);
//                // Init SmtpClient and send
//                var smtpClient = new SmtpClient(server, 587);
//                var credentials = new System.Net.NetworkCredential(userName, password);
//                smtpClient.Credentials = credentials;

//                smtpClient.Send(mailMsg);
//            }
//            catch (Exception ex)
//            {
//                Trace.TraceError(ex.Message + ": " + ex.StackTrace);
//                throw;
//            }
//        }

//        private static IEnumerable<LinkedResource> GetEmailLinkedResources()
//        {
//            // TODO: cache bytes then recreate memory streams for the linkedresources
//            var assembly = typeof(EmailChannelDistribution).Assembly;
//            var topLogoStream = assembly.GetManifestResourceStream("Smart.Notifications.Worker.Channels.Emails.Images.top_logo.jpeg");
//            var bottomLogoStream = assembly.GetManifestResourceStream("Smart.Notifications.Worker.Channels.Emails.Images.bottom_logo.jpeg");

//            var topLogo = new LinkedResource(topLogoStream, new ContentType("image/jpeg"));
//            topLogo.ContentId = "luxuryMarketlogo";
//            var bootomLogo = new LinkedResource(bottomLogoStream, new ContentType("image/jpeg"));
//            bootomLogo.ContentId = "luxuryMarketlogosmall";

//            return new List<LinkedResource> { topLogo, bootomLogo };
//        }

//        public static string GetEmailReceipient(string to, bool sendToRecipient)
//        {
//            if (sendToRecipient)
//                return to;

//            var rerouteAddress = CloudConfigurationManager.GetSetting(NotificationCloudConfigurationKeys.RerouteAddress);
//            return rerouteAddress;
//        }
//    }
//}
