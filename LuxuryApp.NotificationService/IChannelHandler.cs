﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.NotificationService
{
    public interface IChannelHandler<in T> where T : ChannelDistribution
    {
        bool CanHandle(T context);
        void Handle(T context);
    }
}
