﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.NotificationService.Enums
{
    public enum NotificationStatus
    {
        None = 0,

        Failed = 1,

        SentToProvider = 2,

        Delivered = 3,

        DeliveryFailed = 4,

        ReadAtDestination = 5
    }
}
