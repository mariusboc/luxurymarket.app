﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuxuryApp.NotificationService
{
    public abstract class ChannelDistribution
    {
        /// <summary>
        /// All channels should take this flag into consideration. If this is false the system is in debug mode.
        /// </summary>
        public bool IsActive { get; set; }

    }
}
