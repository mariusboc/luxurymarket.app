﻿using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using LuxuryApp.Core.Infrastructure.Handlers;
using System.Web.Http.ExceptionHandling;
using LuxuryApp.Core.Infrastructure.Api.Loggers;

namespace LuxuryApp.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
          
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            var json = config.Formatters.JsonFormatter;
            json.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            json.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.None;
            json.SerializerSettings.Formatting = Formatting.None;
            json.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
            json.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            json.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            //var connectionStringName = ConfigurationManager.AppSettings["ConnectionStringName"];
            //var connectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;

            //container.RegisterType<IRepository, RepositoryAdapter>(new HierarchicalLifetimeManager(),new InjectionConstructor(connectionString));
            //container.RegisterType<ICustomerService, CustomerService>(new HierarchicalLifetimeManager());
            //DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            ////Set WEB API dependency resolver
            //GlobalConfiguration.Configuration.DependencyResolver = new UnityBootstrap(container);

            //// Setup the ambient service locator
            //IServiceLocator locator = new UnityServiceLocator(container);
            //ServiceLocator.SetLocatorProvider(() => locator);

            //exceptions handler and logger
            config.Services.Replace(typeof(IExceptionHandler), new LuxuryExceptionHandler());
            config.Services.Add(typeof(IExceptionLogger), new ApiExceptionLogger());

        }
    }
}
