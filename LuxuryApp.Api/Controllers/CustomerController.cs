﻿//using System;
//using System.Web.Http;
//using LuxuryApp.Contracts.Services;
//using LuxuryApp.Processors.Services;
//using LuxuryApp.Contracts.Models;

//namespace LuxuryApp.Api.Controllers
//{
//    [RoutePrefix("api/customers")]
//    public class CustomerController : ApiController
//    {
//        private ICustomerService _customerServiceFactory;

//        public CustomerController()
//        {
//             _customerServiceFactory = new CustomerService();
//        }

//        public CustomerController(ICustomerService customerServiceFactory)
//        {
//            _customerServiceFactory = customerServiceFactory;
//        }

//        [HttpPost]
//        [Route("")]
//        public IHttpActionResult SaveCustomer(Customer model)
//        {
//            if (!ModelState.IsValid)
//            {
//                throw new Exception("Invalid input");
//            }

//            _customerServiceFactory.CreateCustomer(model);
//            return Ok();
//        }

//        [HttpPost]
//        [Route("request")]
//        public IHttpActionResult SaveRequest(Request model)
//        {
//            if (!ModelState.IsValid)
//            {
//                throw new Exception("Invalid input");
//            }
//            _customerServiceFactory.CreateCustomerRequest(model);
//            return Ok();
//        }

//    }
//}