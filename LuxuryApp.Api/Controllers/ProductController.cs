﻿using System;
using System.Web.Http;
using LuxuryApp.Contracts.Models;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Processors.Services;
using System.Web.Http.Description;

namespace LuxuryApp.Api.Controllers
{
    [RoutePrefix("api/products")]
    public class ProductController : ApiController
    {
        private IProductService _productServiceFactory;

        public ProductController()
        {
            _productServiceFactory = new ProductService();
        }
        public ProductController(IProductService productServiceFactory)
        {
            _productServiceFactory = productServiceFactory;
        }

        [HttpGet]
        [Route("{id:int}")]
        [ResponseType(typeof(Product))]
        public IHttpActionResult GetProductById(int id)
        {
            var product = _productServiceFactory.GetProductById(id);
            return Ok(product);
        }

        [HttpGet]
        [Route("{sku}")]
        [ResponseType(typeof(Product))]
        public IHttpActionResult GetProductBySku(string sku)
        {
            var product = _productServiceFactory.GetProductBySku(sku);
            return Ok(product);
        }


        [HttpPost]
        [Route("Search")]
        [ResponseType(typeof(ProductSearchItems))]
        public IHttpActionResult SearchProduct([FromBody]ProductSearchItems searchItems)
        {
            if (searchItems == null)
                throw new Exception("Search Items required");

            var product = _productServiceFactory.SearchProduct(searchItems);
            return Ok(product);
        }

    }
}