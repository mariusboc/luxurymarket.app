﻿using System;
using System.Web.Http;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Processors.Services;
using LuxuryApp.Contracts.Models;

namespace LuxuryApp.Api.Controllers
{
    [RoutePrefix("api/registrationRequests")]
    [AllowAnonymous]
    public class RegistrationRequestController : ApiController
    {
        private IRegistrationRequestService _requestService;

        public RegistrationRequestController()
        {
            _requestService = new RegistrationRequestService();
        }

        [HttpPost]
        [Route("")]
        public IHttpActionResult SaveRequest(RegistrationRequest model)
        {
            if (!ModelState.IsValid)
            {
                throw new Exception("Invalid input");
            }

            _requestService.CreateRequest(model);
            return Ok();
        }
    }
}