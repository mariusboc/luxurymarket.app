﻿using LuxuryApp.Api.ActionFilterAttributes;
using LuxuryApp.Core.Infrastructure.Providers;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using LuxuryApp.Utils.Helpers;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Processors.Services;
using LuxuryApp.Contracts.Models;

namespace LuxuryApp.Api.Controllers
{
  //  [Authorize]
    [RoutePrefix("api/offers")]
    public class OfferController : ApiController
    {
        #region Private variable.

        private static readonly string _ServerUploadFolder = "~/uploads"; //Path.GetTempPath();

        private readonly IOfferImportService _offerService;

        #endregion

        #region Public Constructor

        /// <summary>
        /// Public constructor to initialize product service instance
        /// </summary>
        public OfferController()
        {
            _offerService = new OfferImportService();
        }

        #endregion

        [Route("upload")]
        [HttpPost]
        [ValidateMimeMultipartContentFilter]
        public async Task<OfferImport> UploadFile()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid Request!");
                throw new HttpResponseException(response);
            }

            string uploadPath = HttpContext.Current.Server.MapPath(_ServerUploadFolder);
            var streamProvider = new ImportProvider(uploadPath);
            var task = await Request.Content.ReadAsMultipartAsync(streamProvider);
            var fileName = streamProvider.FileData[0].LocalFileName;

            var result = _offerService.ParseOfferImport(fileName);
            return result;
        }
    }
}