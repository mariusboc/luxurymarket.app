﻿using System;
using System.Web.Http;
using LuxuryApp.Contracts.Services;
using LuxuryApp.Processors.Services;
using LuxuryApp.Contracts.Models;

namespace LuxuryApp.Api.Controllers
{
    [RoutePrefix("api/materials")]
    public class MaterialController : ApiController
    {
        private MaterialService _materialServer;

        public MaterialController()
        {
            _materialServer = new MaterialService();
        }


        [HttpGet]
        [Route("")]
        public IHttpActionResult Get()
        {
            var entities = _materialServer.Search();
            return Ok(entities);
        }

        [HttpGet]
        [Route("{materialId:int}")]
        public IHttpActionResult GetById(int MaterialId)
        {
            var entities = _materialServer.Search(materialId: MaterialId);
            return Ok(entities);
        }

        [HttpGet]
        [Route("brand/{brandId:int}")]
        public IHttpActionResult GetByBrand(int brandId)
        {
            var entities = _materialServer.Search(brandId: brandId);
            return Ok(entities);
        }

        [HttpGet]
        [Route("materialCode/{materialCode}")]
        public IHttpActionResult GetByMaterialCode(string materialCode)
        {
            var entities = _materialServer.Search(materialCode: materialCode);
            return Ok(entities);
        }
    }
}