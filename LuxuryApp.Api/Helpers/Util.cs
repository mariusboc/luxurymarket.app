﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Resources;

namespace LuxuryApp.Api.Helpers
{
    public static class Util
    {
        public static string AppendTimeStamp(this string fileName)
        {
            return string.Concat(
                Path.GetFileNameWithoutExtension(fileName),
                DateTime.Now.ToString("yyyyMMddHHmmssfff"),
                Path.GetExtension(fileName)
                );
        }

        public static Dictionary<string, string> GetExpectedImportColumns(string resourcePath)
        {
            using (var rsxr = new ResXResourceReader(resourcePath))
            {
                var columns = new Dictionary<string, string>();

                foreach (DictionaryEntry d in rsxr)
                {
                    columns.Add(d.Key.ToString(), d.Value.ToString().Trim().ToLower());
                }

                return columns;
            }
        }

    }
}